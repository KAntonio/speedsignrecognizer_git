﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.Util;
using Emgu.CV.CvEnum;
using Emgu.CV.UI;
using RoadSignRecognizer.domain;
using Emgu.CV.OCR;
using Aspose.OCR;
using CameraCapture.repository;
using System.Threading;
using Emgu.CV.Util;

using CameraCapture.domain;
using System.Diagnostics;

namespace RoadSignRecognizer
{
    public partial class SpeedSignRecognizer : Form
    {
        Repository repo;
        OcrEngine ocr;

        //declaring global variables
        private Capture capture;        //takes images from camera as image frames
        private bool captureInProgress;

        private CascadeClassifier haarSpeedSign;  //  the viola-jones classifier (detector)
        private CascadeClassifier haarStopSign;

        // declaring parameters variables
        private int WindowsSizeMin ;
        private int WindowsSizeMax ;
        private Double ScaleIncreaseRate ;
        private int MinNeighbors ;
        private int MinNeighborsStop;

        private int MinHue;
        private int MaxHue;
        private int MinSaturation;
        private int MaxSaturation;
        private int MinValue;
        private int MaxValue;

        //circle
        private int cannyTresh;
        private int circleAccTresh;
        private int resAccCenter;
        private int minDistanceCenters;

        //line
        private int cannyMinThreshold;
        private int cannyThresholdLinking;
        private int thresholdHoughLines;
        private int minLineWidth;
        private int gapBetweenLines;


        Image<Bgr, Byte> ImageFrame;

        int contorCreareOcr = 0;

        private List<CircleF> listOfAllCircles;
        private List<Triangle2DF> triangleList ;
        private List<RotatedRect> boxList;
        private List<LineSegment2D> linesList;


        public SpeedSignRecognizer()
        {
            InitializeComponent();

        }

        //------------------------------------------------------------------------------//
        //Process Frame() below is our user defined function in which we will create an EmguCv 
        //type image called ImageFrame. capture a frame from camera and allocate it to our 
        //ImageFrame. then show this image in ourEmguCV imageBox
        //------------------------------------------------------------------------------//
        private void ProcessFrame(object sender, EventArgs arg)
        {
            
            ImageFrame = capture.QueryFrame().ToImage<Bgr, Byte>();  // iau de la WebCam

            
            if (ImageFrame != null)
            {
               

                   // take the values of parameters from the GUI
                   MinHue = tkBar_MinimumHue.Value;
                MaxHue = tkBar_MaximumHue.Value;
                MinSaturation = tkBar_MinimumSaturation.Value;
                MaxSaturation = tkBar_MaximumSaturation.Value;
                MinValue = tkBar_MinimumValue.Value;
                MaxValue = tkBar_MaximumValue.Value;
                cannyTresh = tkBar_CannyTreshold.Value;
                circleAccTresh = tkBar_CircleAccumulatorTreshold.Value;
                resAccCenter = tkBar_RessAccCenter.Value;
                minDistanceCenters = tkBar_MinDistanceBtwCenters.Value;

                cannyMinThreshold = tkBar_CannyMinThresholdLines.Value;
                cannyThresholdLinking = tkBar_CannyThresholdLinkingLines.Value;
                thresholdHoughLines = tkBar_ThresholdHoughLines.Value;
                minLineWidth = tkBar_MinLineLength.Value;
                gapBetweenLines = tkBar_GapBetweenLines.Value;
               

                // 1. Convert the image to HSV
                using (Image<Hsv, byte> hsv = ImageFrame.Convert<Hsv, byte>())
                {
                    // 2. Obtain the 3 channels (hue, saturation and value) that compose the HSV image
                    Image<Gray, byte>[] channels = hsv.Split();

                    try
                    {
                        // 3. Remove all pixels from the hue channel that are not in the range [40, 60]
                        CvInvoke.InRange(channels[0], new ScalarArray(new Gray(MinHue).MCvScalar), new ScalarArray(new Gray(MaxHue).MCvScalar), channels[0]);  // 180 max
                        CvInvoke.InRange(channels[1], new ScalarArray(new Gray(MinSaturation).MCvScalar), new ScalarArray(new Gray(MaxSaturation).MCvScalar), channels[1]);  //255 max
                        CvInvoke.InRange(channels[2], new ScalarArray(new Gray(MinValue).MCvScalar), new ScalarArray(new Gray(MaxValue).MCvScalar), channels[2]);  // 255 max

                        // 4. Display the result
                        pictureBox_Hue.Image = channels[0].ToBitmap();
                        pictureBox_Saturation.Image = channels[1].ToBitmap();
                        pictureBox_Value.Image = channels[2].ToBitmap();



                        // TRIANGLES AND BOXES (roatated rectangles)
                        for(int z = 0; z<=2; z++) // for each channel
                        {
                            #region Canny and edge detection for detecting the lines 


                            UMat cannyEdges = new UMat();
                            CvInvoke.Canny(channels[z], cannyEdges, cannyMinThreshold, cannyThresholdLinking);    // APPLYING CANNY IN SATURATION FILTER channels[1]


                            LineSegment2D[] lines = CvInvoke.HoughLinesP(
                               cannyEdges,
                               1, //Distance resolution in pixel-related units 1 
                               Math.PI / 45.0, //Angle resolution measured in radians.
                               thresholdHoughLines, //threshold 20
                               minLineWidth, //min Line width 30
                               gapBetweenLines); //gap between lines 10

                            for (int i = 0; i < lines.Length; i++)
                            {
                                linesList.Add(lines[i]);
                            }



                            #endregion
                            #region Find triangles and rectangles 
                            Stopwatch watchTriangleAndREctangle = Stopwatch.StartNew();
                            using (VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint())
                            {
                                CvInvoke.FindContours(cannyEdges, contours, null, RetrType.List, ChainApproxMethod.ChainApproxSimple);
                                int count = contours.Size;
                                for (int i = 0; i < count; i++)
                                {
                                    using (VectorOfPoint contour = contours[i])
                                    using (VectorOfPoint approxContour = new VectorOfPoint())
                                    {
                                        CvInvoke.ApproxPolyDP(contour, approxContour, CvInvoke.ArcLength(contour, true) * 0.05, true);
                                        if (CvInvoke.ContourArea(approxContour, false) > 30) //only consider contours with area greater than 250
                                        {
                                            if (approxContour.Size == 3) //The contour has 3 vertices, it is a triangle
                                            {
                                                bool isTriangle = true;
                                                Point[] pts = approxContour.ToArray();

                                                LineSegment2D[] edges = PointCollection.PolyLine(pts, true);
                                                for (int j = 0; j < edges.Length; j++)
                                                {
                                                    double angle = Math.Abs(edges[(j + 1) % edges.Length].GetExteriorAngleDegree(edges[j]));
                                                    if (angle < 115 || angle > 125)
                                                    {
                                                        isTriangle = false;
                                                        break;
                                                    }
                                                }

                                                if (isTriangle == true)
                                                {
                                                    triangleList.Add(new Triangle2DF(  // create a triangle and add it in the list
                                                         pts[0],
                                                         pts[1],
                                                         pts[2]
                                                        ));
                                                    Triangle2DF tr = new Triangle2DF(pts[0], pts[1], pts[2]);
                                                    int nrCornersHigh = 0;
                                                    for (int k = 0; k <= 2; k++)
                                                    {
                                                        if (pts[k].Y < tr.Centeroid.Y)
                                                        {
                                                            nrCornersHigh++;
                                                        }
                                                    }

                                                    if (nrCornersHigh == 2)  // if 2 corners are higher than the center
                                                    {
                                                        GiveWaySign giveWaySign = new GiveWaySign();  // create a new Give Way sign
                                                        repo.AddGiveWaySign(giveWaySign);   // add teh sign into the repo
                                                    }

                                                }
                                            }
                                            else if (approxContour.Size == 4) //The contour has 4 vertices.
                                            {
                                                #region determine if all the angles in the contour are within [80, 100] degree
                                                bool isRectangle = true;
                                                Point[] pts = approxContour.ToArray();
                                                LineSegment2D[] edges = PointCollection.PolyLine(pts, true);

                                                for (int j = 0; j < edges.Length; j++)
                                                {
                                                    double angle = Math.Abs(
                                                       edges[(j + 1) % edges.Length].GetExteriorAngleDegree(edges[j]));
                                                    if (angle < 80 || angle > 100)
                                                    {
                                                        isRectangle = false;
                                                        break;
                                                    }
                                                }
                                                #endregion

                                                if (isRectangle)
                                                {

                                                    RotatedRect r = CvInvoke.MinAreaRect(approxContour);
                                                    double latura1 = Math.Sqrt(Math.Pow(r.GetVertices().ElementAt(1).X - r.GetVertices().ElementAt(0).X, 2) + Math.Pow(r.GetVertices().ElementAt(1).Y - r.GetVertices().ElementAt(0).Y, 2));
                                                    double latura2 = Math.Sqrt(Math.Pow(r.GetVertices().ElementAt(2).X - r.GetVertices().ElementAt(1).X, 2) + Math.Pow(r.GetVertices().ElementAt(2).Y - r.GetVertices().ElementAt(1).Y, 2));
                                                    if (latura1 * 3 < latura2 || latura2 * 3 < latura1)
                                                    {
                                                        boxList.Add(CvInvoke.MinAreaRect(approxContour));
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            watchTriangleAndREctangle.Stop();
                            Console.WriteLine(watchTriangleAndREctangle.ElapsedMilliseconds + " pentru triunghiuri si dreptunghiuri");
                            #endregion
                        }



                        #region manage repo boolean for GIVE WAY SIGN (triangle)

                        repo.SetBoolFoundGiveWaySign();  // setting true the bool of a found Give way sign if it is the case

                        if (repo.found_giveWay == true)
                        {
                            pictureBox_giveWay.Visible = true;
                            System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
                            timer.Interval = 2000;
                            timer.Tick += (source, e) => { pictureBox_giveWay.Visible = false; timer.Stop(); };
                            timer.Start();
                        }
                        #endregion

                        if (checkBox_RectanglesAndTriangles.Checked)
                        {
                            #region draw triangles and rectangles 
                            foreach (Triangle2DF triangle in triangleList)
                            {
                                ImageFrame.Draw(triangle, new Bgr(Color.Orange), 10);
                            }
                            foreach (RotatedRect box in boxList)
                                ImageFrame.Draw(box, new Bgr(Color.Red), 10);
                            #endregion
                        }


                        #region draw lines
                        if (checkBox_lines.Checked)
                        {
                            foreach (LineSegment2D line in linesList)
                                ImageFrame.Draw(line, new Bgr(Color.Pink), 5);
                        }
                        #endregion


                        #region detect circles
                        Stopwatch watchCircles = Stopwatch.StartNew();

                        CvInvoke.GaussianBlur(channels[0], channels[0], new System.Drawing.Size(13, 13), 5.5, 5.5);
                        CvInvoke.GaussianBlur(channels[1], channels[1], new System.Drawing.Size(13, 13), 5.5, 5.5);
                        CvInvoke.GaussianBlur(channels[2], channels[2], new System.Drawing.Size(13, 13), 5.5, 5.5);

                        Gray cannyThreshold = new Gray(cannyTresh);  // 180                                     
                        Gray circleAccumulatorThreshold = new Gray(circleAccTresh);  //120

                        for(int m = 0; m<= 2; m++)
                        {
                            CircleF[] circles = channels[m].HoughCircles(
                            cannyThreshold,
                            circleAccumulatorThreshold,
                            0.1 * resAccCenter, //Resolution of the accumulator used to detect centers of the circles  era 5.0
                            1.0 * minDistanceCenters, //min distance  era 10.0
                            5, //min radius era 5
                            70 //max radius era 0
                            )[0]; //Get the circles from the first channel

                            for (int i = 0; i < circles.Length; i++)
                            {
                                listOfAllCircles.Add(circles[i]);
                            }
                        }

                        watchCircles.Stop();
                        Console.WriteLine(watchCircles.ElapsedMilliseconds + " pentrucercuri la forme");
                        #endregion


                        #region search for NO ENTRY signs ( combination of circle with box inside)
                        if (listOfAllCircles.Count > 0 && boxList.Count > 0) // if we found both circles and rectangles
                        {
                            foreach (CircleF circle in listOfAllCircles)
                            {
                                foreach(RotatedRect rectangle in boxList)
                                {
                                    if(  Math.Sqrt(  Math.Pow(rectangle.Center.X - circle.Center.X,2) + Math.Pow(rectangle.Center.Y - circle.Center.Y, 2)) < 20){ // the distance between the center of circle and rectangle
                                        NoEntrySign sign = new NoEntrySign();
                                        repo.AddNoEntrySign(sign);
                                    }
                                }
                            }
                        }

                        #endregion

                        #region manage repo boolean for NO ENTRY sign
                        repo.SetBoolFoundNoEntrySign();  // setting true the bool of a found NO ENTRY sign if it is the case

                        if (repo.found_noEntry == true)
                        {
                            pictureBox_interzis.Visible = true;
                            System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
                            timer.Interval = 2000;
                            timer.Tick += (source, e) => { pictureBox_interzis.Visible = false; timer.Stop(); };
                            timer.Start();
                        }
                        #endregion

                        #region draw circles
                        if (checkBox_HSV.Checked)
                        {
                            foreach (var circle in listOfAllCircles)
                            {
                                ImageFrame.Draw(circle, new Bgr(Color.Red), 5); // draw the circles
                            }
                            
                        }

                        #endregion

                    }
                    finally
                    {
                        channels[0].Dispose();
                        channels[1].Dispose();
                        channels[2].Dispose();
                        listOfAllCircles.Clear();
                        triangleList.Clear();
                        boxList.Clear();
                        linesList.Clear();
                    }
                }






                // ----------------------------------------------------------- HAAR SPEED SIGNS + STOP signs-------------------------------------------------

                #region detection + recognition using HAAR
                if (checkBox_HaarCascadeForSpeedSigns.Checked)
                {
                    // Console.WriteLine(".....  " + ImageFrame.Height + " " + ImageFrame.Width);  // = 480 x 640
                    Image<Gray, byte> grayframe = ImageFrame.Convert<Gray, byte>(); // transform fame-ul in alb-negru pentru a putea fi prelucrat

                    // take the values of parameters from the GUI
                    
                    WindowsSizeMin = tkBar_MinumumDetectionScale.Value;
                    WindowsSizeMax = tkBar_MaximumDetectionScale.Value;
                    MinNeighbors = tkBar_MinimumNeighbors.Value;
                    MinNeighborsStop = tkBar_MinimumNeighborsSTOP.Value;
                    ScaleIncreaseRate = Double.Parse(comboBoxScaleIncRate.Text);

                    Stopwatch watchStop = Stopwatch.StartNew();
                    var stopSigns = haarStopSign.DetectMultiScale(   // apply Viola Jones algorithm to detect the speed signs
                        grayframe,   // the original frame transformed to gray image
                        ScaleIncreaseRate,  // how much the scale from Min to Max will increase while searching
                        MinNeighborsStop,  // how many neighbors need to be such that the sighn is a valid one
                        new Size(WindowsSizeMin, WindowsSizeMin),  // minimum size of the sign
                        new Size(WindowsSizeMax, WindowsSizeMax));  // maimum size of the sign
                    watchStop.Stop();
                    Console.WriteLine(watchStop.ElapsedMilliseconds + " pentru semele de stop");
                    foreach (var stop in stopSigns)
                    {
                        ImageFrame.Draw(stop, new Bgr(Color.Yellow), 4);  // desenez stopul 
                        repo.AddStopSign(new StopSign());
                    }


                    // DETECTEZ SEMNELE de viteaza
                    Stopwatch watchSpeed = Stopwatch.StartNew();
                    var signs = haarSpeedSign.DetectMultiScale(   // apply Viola Jones algorithm to detect the speed signs
                        grayframe,   // the original frame transformed to gray image
                        ScaleIncreaseRate,  // how much the scale from Min to Max will increase while searching
                        MinNeighbors,  // how many neighbors need to be such that the sighn is a valid one
                        new Size(WindowsSizeMin, WindowsSizeMin),  // minimum size of the sign
                        new Size(WindowsSizeMax, WindowsSizeMax));  // maimum size of the sign
                    watchSpeed.Stop();
                    Console.WriteLine(watchSpeed.ElapsedMilliseconds + " pentru semele de viteza");
                    // PARCURG SEMNELE
                    foreach (var sign in signs) // pentru feicare semn gasit desenez dreptunghiul
                    {
                        ImageFrame.Draw(sign, new Bgr(Color.Red), 2);  // desenez semnul 

                        SpeedLimitSign speedSign = new SpeedLimitSign(); // create object of type SpeedLimitSign

                        // SCHIMB ROI DOAR PE SEMNUL GASIT
                        var rect = new System.Drawing.Rectangle(sign.X, sign.Y, sign.Width, sign.Height); // setez dreptunghiul
                        ImageFrame.ROI = rect;   // aplic dreptunghiul ca fiind ROI

                        Image<Bgr, Byte> img = new Image<Bgr, Byte>(ImageFrame.ToBitmap());   // imaginea color a semnului


                        Image<Gray, byte> myImage = new Image<Gray, byte>(img.ToBitmap());  // imaginea Gray a semnului (pt a aplica Canny)

                        //CvInvoke.GaussianBlur(myImage, myImage, new System.Drawing.Size(5, 5), 1.5, 1.5);

                        Gray cannyThreshold = new Gray(150);  // 300                                     
                        Gray circleAccumulatorThreshold = new Gray(60);  //120

                        Stopwatch watchCirclesInHaar = Stopwatch.StartNew();
                        CircleF[] circles = myImage.HoughCircles(
                            cannyThreshold,
                            circleAccumulatorThreshold,
                            0.2, //Resolution of the accumulator used to detect centers of the circles  era 5.0
                            20.0, //min distance  era 10.0
                            5, //min radius era 5
                            0 //max radius era 0
                            )[0]; //Get the circles from the first channel
                        watchCirclesInHaar.Stop();
                        Console.WriteLine(watchCirclesInHaar.ElapsedMilliseconds + " cercurile din Haar");
                        speedSign.AllocatePossibleSpeeds(circles.Length);  // allocate memory for as many possible values of speed
                                                                           //as circles detected

                        pictureBox_ColoredCroppedSign.Image = img.ToBitmap();
                        CvInvoke.cvResetImageROI(ImageFrame);  // reset region of interest of the big original frame 


                        #region circles
                        foreach (var circle in circles)
                        {
                            img.Draw(circle, new Bgr(Color.Green), 2); // draw the circles
                            pictureBox_ColoredCroppedSign.Image = img.ToBitmap();
                            int radius = (int)circles[0].Radius;

                            int x = (int)circles[0].Center.X - radius;
                            int y = (int)circles[0].Center.Y - radius;
                            //Console.WriteLine("x: " + x + " y: " + y + " center: " + circles[0].Center + " radius: " + radius);   // stiu ca se refera la coordonatele din imaginea mica (imageBox4)


                            Bitmap tmp = new Bitmap(2 * radius, 2 * radius);

                            Graphics g = Graphics.FromImage(tmp);
                            g.TranslateTransform(tmp.Width / 2, tmp.Height / 2); // asa il aseaza bine in stanga sus

                            GraphicsPath path = new GraphicsPath();
                            path.AddEllipse(0 - radius, 0 - radius, 2 * radius, 2 * radius);
                            Region region = new Region(path);
                            g.SetClip(region, CombineMode.Replace);

                            Bitmap bmp = myImage.ToBitmap();
                          
                            g.DrawImage(bmp, new Rectangle(-radius * 2, -radius * 2, 4 * radius, 4 * radius), new Rectangle(x - radius + radius / 4, y - radius + radius / 4, 4 * radius - radius / 2, 4 * radius - radius / 2), GraphicsUnit.Pixel);

                            pictureBox_cropped_circle.Image = tmp;
                            tmp.Save("detected_circle/circle.jpg");


                            contorCreareOcr++;
                            if (contorCreareOcr == 6)
                            {

                                ocr = new OcrEngine();
                                contorCreareOcr = 0;
                            }

                            ocr.Image = ImageStream.FromFile("detected_circle/circle.jpg");
                            if (ocr.Process())
                            {
                               // Console.WriteLine("OCR:>>" + ocr.Text.ToString());
                                speedSign.AddPossibleSpeed(ocr.Text.ToString());
                            }


                        } // endforeach circles
                        #endregion end circles


                        repo.AddSpeedSign(speedSign);  // after i add all possible speeds as strings in the object, i put it in repo list
                    } // end foreach(sign in signs)

                    repo.setBoolFoundStopSign();
                    repo.SetBoolsFoundSpeedSigns();   // set the booleans in repo for speed signs

                    // based on booleans from repo, maxe picture boxes visible
                    #region make the picture boxes visible
                    if (repo.found_10 == true)
                    {
                        pictureBox_limit_10.Visible = true;
                        System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
                        timer.Interval = 2000;
                        timer.Tick += (source, e) => { pictureBox_limit_10.Visible = false; timer.Stop(); };
                        timer.Start();
                    }
                    if (repo.found_30 == true)
                    {
                        pictureBox_limit_30.Visible = true;
                        System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
                        timer.Interval = 2000;
                        timer.Tick += (source, e) => { pictureBox_limit_30.Visible = false; timer.Stop(); };
                        timer.Start();
                    }
                    if (repo.found_50 == true)
                    {
                        pictureBox_limit_50.Visible = true;
                        System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
                        timer.Interval = 2000;
                        timer.Tick += (source, e) => { pictureBox_limit_50.Visible = false; timer.Stop(); };
                        timer.Start();

                    }
                    if (repo.found_70 == true)
                    {
                        pictureBox_limit_70.Visible = true;
                        System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
                        timer.Interval = 2000;
                        timer.Tick += (source, e) => { pictureBox_limit_70.Visible = false; timer.Stop(); };
                        timer.Start();

                    }
                    if (repo.found_90 == true)
                    {
                        pictureBox_limit_90.Visible = true;
                        System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
                        timer.Interval = 2000;
                        timer.Tick += (source, e) => { pictureBox_limit_90.Visible = false; timer.Stop(); };
                        timer.Start();

                    }
                    if (repo.found_100 == true)
                    {
                        pictureBox_limit_100.Visible = true;
                        System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
                        timer.Interval = 2000;
                        timer.Tick += (source, e) => { pictureBox_limit_100.Visible = false; timer.Stop(); };
                        timer.Start();

                    }
                    if (repo.found_120 == true)
                    {
                        pictureBox_limit_120.Visible = true;
                        System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
                        timer.Interval = 2000;
                        timer.Tick += (source, e) => { pictureBox_limit_120.Visible = false; timer.Stop(); };
                        timer.Start();
                    }
                    if (repo.found_130 == true)
                    {
                        pictureBox_limit_130.Visible = true;
                        System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
                        timer.Interval = 2000;
                        timer.Tick += (source, e) => { pictureBox_limit_130.Visible = false; timer.Stop(); };
                        timer.Start();
                    }
                    if (repo.found_stop == true)
                    {
                        pictureBox_stop.Visible = true;
                        System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
                        timer.Interval = 2000;
                        timer.Tick += (source, e) => { pictureBox_stop.Visible = false; timer.Stop(); };
                        timer.Start();
                    }

                    #endregion



                }

                #endregion detect + recog using Haar
               
                
                // reset the repository for the next frame
                repo.ResetRepository();


                CamImageBox.Image = ImageFrame; // refrest the displayed frame       

                
            }
        }


     
        //btnStart_Click() function is the one that handles our "Start!" button' click 
        //event. it creates a new capture object if its not created already. e.g at first time
        //starting. once the capture is created, it checks if the capture is still in progress,
        //if so the
        private void btnStart_Click(object sender, EventArgs e)
        {
            #region if capture is not created, create it now
            if (capture == null)
            {
                try
                {
                    capture = new Capture();
                }
                catch (NullReferenceException excpt)
                {
                    MessageBox.Show(excpt.Message);
                }
            }
            #endregion

            if (capture != null)
            {
                if (captureInProgress)
                {  //if camera is getting frames then stop the capture and set button Text
                    // "Start" for resuming capture
                    btnStart.Text = "Start webcam!"; //
                    Application.Idle -= ProcessFrame;
                }
                else
                {
                    //if camera is NOT getting frames then start the capture and set button
                    // Text to "Stop" for pausing capture
                    btnStart.Text = "Stop webcam!";
                    Application.Idle += ProcessFrame;
                }

                captureInProgress = !captureInProgress;
            }
        }




        private void ReleaseData()
        {
            if (capture != null)
                capture.Dispose();
        }


        private void RoadSignRecognizer_Load(object sender, EventArgs e)
        {
            ocr = new OcrEngine();
            repo = new Repository();
            haarSpeedSign = new CascadeClassifier("haarcascade_sign.xml");
            //haarStopSign = new CascadeClassifier("frontal_stop_sign_cascade.xml");
            haarStopSign = new CascadeClassifier("stopsign_classifier.xml");

            // create a list where to keep all the found cirlces from all filters
            listOfAllCircles = new List<CircleF>();
            triangleList = new List<Triangle2DF>();
            boxList = new List<RotatedRect>();
            linesList = new List<LineSegment2D>();
        }


        /// <summary>
        /// The function applied on the loaded images from the disk
        /// </summary>
        public void DetectSigns()
        {
            
            // take the values of parameters from the GUI
            MinHue = tkBar_MinimumHue.Value;
            MaxHue = tkBar_MaximumHue.Value;
            MinSaturation = tkBar_MinimumSaturation.Value;
            MaxSaturation = tkBar_MaximumSaturation.Value;
            MinValue = tkBar_MinimumValue.Value;
            MaxValue = tkBar_MaximumValue.Value;
            cannyTresh = tkBar_CannyTreshold.Value;
            circleAccTresh = tkBar_CircleAccumulatorTreshold.Value;
            resAccCenter = tkBar_RessAccCenter.Value;
            minDistanceCenters = tkBar_MinDistanceBtwCenters.Value;

            cannyMinThreshold = tkBar_CannyMinThresholdLines.Value;
            cannyThresholdLinking = tkBar_CannyThresholdLinkingLines.Value;
            thresholdHoughLines = tkBar_ThresholdHoughLines.Value;
            minLineWidth = tkBar_MinLineLength.Value;
            gapBetweenLines = tkBar_GapBetweenLines.Value;


            #region reset all pincture boxes to invisible
            pictureBox_giveWay.Visible = false;
            pictureBox_interzis.Visible = false;
            pictureBox_interzis_ambele.Visible = false;
            pictureBox_limit_10.Visible = false;
            pictureBox_limit_30.Visible = false;
            pictureBox_limit_50.Visible = false;
            pictureBox_limit_70.Visible = false;
            pictureBox_limit_90.Visible = false;
            pictureBox_limit_100.Visible = false;
            pictureBox_limit_120.Visible = false;
            pictureBox_limit_130.Visible = false;
            pictureBox_sfarsit_restrictii.Visible = false;
            pictureBox_stop.Visible = false;
            #endregion

            // 1. Convert the image to HSV
            using (Image<Hsv, byte> hsv = ImageFrame.Convert<Hsv, byte>())
            {
                // 2. Obtain the 3 channels (hue, saturation and value) that compose the HSV image
                Image<Gray, byte>[] channels = hsv.Split();

                try
                {
                    // 3. Remove all pixels from the hue channel that are not in the range [40, 60]
                    CvInvoke.InRange(channels[0], new ScalarArray(new Gray(MinHue).MCvScalar), new ScalarArray(new Gray(MaxHue).MCvScalar), channels[0]);  // 180 max
                    CvInvoke.InRange(channels[1], new ScalarArray(new Gray(MinSaturation).MCvScalar), new ScalarArray(new Gray(MaxSaturation).MCvScalar), channels[1]);  //255 max
                    CvInvoke.InRange(channels[2], new ScalarArray(new Gray(MinValue).MCvScalar), new ScalarArray(new Gray(MaxValue).MCvScalar), channels[2]);  // 255 max

                    // 4. Display the result
                    pictureBox_Hue.Image = channels[0].ToBitmap();
                    pictureBox_Saturation.Image = channels[1].ToBitmap();
                    pictureBox_Value.Image = channels[2].ToBitmap();



                    // TRIANGLES AND BOXES (roatated rectangles)


                    //-------------------------------------------------HUE----------------------------------------------
                    for (int z = 0; z <= 2; z++)// for each channel
                    {
                        #region Canny and edge detection for detecting the lines  


                        UMat cannyEdges = new UMat();
                        CvInvoke.Canny(channels[z], cannyEdges, cannyMinThreshold, cannyThresholdLinking);    // APPLYING CANNY IN SATURATION FILTER channels[1]


                        LineSegment2D[] lines = CvInvoke.HoughLinesP(
                           cannyEdges,
                           1, //Distance resolution in pixel-related units 1 
                           Math.PI / 45.0, //Angle resolution measured in radians.
                           thresholdHoughLines, //threshold 20
                           minLineWidth, //min Line width 30
                           gapBetweenLines); //gap between lines 10

                        for (int i = 0; i < lines.Length; i++)
                        {
                            linesList.Add(lines[i]);
                        }



                        #endregion
                        #region Find triangles and rectangles IN HUE

                        using (VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint())
                        {
                            CvInvoke.FindContours(cannyEdges, contours, null, RetrType.List, ChainApproxMethod.ChainApproxSimple);
                            int count = contours.Size;
                            for (int i = 0; i < count; i++)
                            {
                                using (VectorOfPoint contour = contours[i])
                                using (VectorOfPoint approxContour = new VectorOfPoint())
                                {
                                    CvInvoke.ApproxPolyDP(contour, approxContour, CvInvoke.ArcLength(contour, true) * 0.05, true);
                                    if (CvInvoke.ContourArea(approxContour, false) > 35) //only consider contours with area greater than 250
                                    {
                                        if (approxContour.Size == 3) //The contour has 3 vertices, it is a triangle
                                        {
                                            bool isTriangle = true;
                                            Point[] pts = approxContour.ToArray();

                                            LineSegment2D[] edges = PointCollection.PolyLine(pts, true);
                                            for (int j = 0; j < edges.Length; j++)
                                            {
                                                double angle = Math.Abs(edges[(j + 1) % edges.Length].GetExteriorAngleDegree(edges[j]));
                                                if (angle < 110 || angle > 130)
                                                {
                                                    isTriangle = false;
                                                    break;
                                                }
                                            }

                                            if (isTriangle == true)
                                            {
                                                triangleList.Add(new Triangle2DF(  // create a triangle and add it in the list
                                                     pts[0],
                                                     pts[1],
                                                     pts[2]
                                                    ));
                                                Triangle2DF tr = new Triangle2DF(pts[0], pts[1], pts[2]);
                                                int nrCornersHigh = 0;
                                                for (int k = 0; k <= 2; k++)
                                                {
                                                    if (pts[k].Y < tr.Centeroid.Y)
                                                    {
                                                        nrCornersHigh++;
                                                    }
                                                }

                                                if (nrCornersHigh == 2)  // if 2 corners are higher than the center
                                                {
                                                    GiveWaySign giveWaySign = new GiveWaySign();  // create a new Give Way sign
                                                    repo.AddGiveWaySign(giveWaySign);   // add teh sign into the repo
                                                }
                                            }
                                        }
                                        else if (approxContour.Size == 4) //The contour has 4 vertices.
                                        {
                                            #region determine if all the angles in the contour are within [80, 100] degree
                                            bool isRectangle = true;
                                            Point[] pts = approxContour.ToArray();
                                            LineSegment2D[] edges = PointCollection.PolyLine(pts, true);

                                            for (int j = 0; j < edges.Length; j++)
                                            {
                                                double angle = Math.Abs(
                                                   edges[(j + 1) % edges.Length].GetExteriorAngleDegree(edges[j]));
                                                if (angle < 80 || angle > 100)
                                                {
                                                    isRectangle = false;
                                                    break;
                                                }
                                            }
                                            #endregion

                                            if (isRectangle)
                                            {

                                                RotatedRect r = CvInvoke.MinAreaRect(approxContour);
                                                double latura1 = Math.Sqrt(Math.Pow(r.GetVertices().ElementAt(1).X - r.GetVertices().ElementAt(0).X, 2) + Math.Pow(r.GetVertices().ElementAt(1).Y - r.GetVertices().ElementAt(0).Y, 2));
                                                double latura2 = Math.Sqrt(Math.Pow(r.GetVertices().ElementAt(2).X - r.GetVertices().ElementAt(1).X, 2) + Math.Pow(r.GetVertices().ElementAt(2).Y - r.GetVertices().ElementAt(1).Y, 2));
                                                if (latura1 * 3 < latura2 || latura2 * 3 < latura1)
                                                {
                                                    boxList.Add(CvInvoke.MinAreaRect(approxContour));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        #endregion
                    }

                    #region manage repo boolean for GIVE WAY SIGN (triangle)

                    repo.SetBoolFoundGiveWaySign();  // setting true the bool of a found Give way sign if it is the case

                    if (repo.found_giveWay == true)
                    {
                        pictureBox_giveWay.Visible = true;
                    }
                    #endregion

                    if (checkBox_RectanglesAndTriangles.Checked)
                    {
                        #region draw triangles and rectangles 
                        foreach (Triangle2DF triangle in triangleList)
                        {
                            ImageFrame.Draw(triangle, new Bgr(Color.Orange), 10);
                        }
                        foreach (RotatedRect box in boxList)
                            ImageFrame.Draw(box, new Bgr(Color.Red), 10);
                        #endregion
                    }


                    #region draw lines
                    if (checkBox_lines.Checked)
                    {
                        foreach (LineSegment2D line in linesList)
                            ImageFrame.Draw(line, new Bgr(Color.Pink), 5);
                    }
                    #endregion


                    #region detect circles



                    CvInvoke.GaussianBlur(channels[0], channels[0], new System.Drawing.Size(13, 13), 5.5, 5.5);
                    CvInvoke.GaussianBlur(channels[1], channels[1], new System.Drawing.Size(13, 13), 5.5, 5.5);
                    CvInvoke.GaussianBlur(channels[2], channels[2], new System.Drawing.Size(13, 13), 5.5, 5.5);

                    Gray cannyThreshold = new Gray(cannyTresh);  // 180                                     
                    Gray circleAccumulatorThreshold = new Gray(circleAccTresh);  //120


                    for (int m = 0; m <= 2; m++)
                    {
                        CircleF[] circles = channels[m].HoughCircles(
                        cannyThreshold,
                        circleAccumulatorThreshold,
                        0.1 * resAccCenter, //Resolution of the accumulator used to detect centers of the circles  era 5.0
                        1.0 * minDistanceCenters, //min distance  era 10.0
                        5, //min radius era 5
                        0 //max radius era 0
                        )[0]; //Get the circles from the first channel

                        for (int i = 0; i < circles.Length; i++)
                        {
                            listOfAllCircles.Add(circles[i]);
                        }
                    }
                    #endregion


                    #region search for NO ENTRY signs ( combination of circle with box inside)
                    if (listOfAllCircles.Count > 0 && boxList.Count > 0) // if we found both circles and rectangles
                    {
                        foreach (CircleF circle in listOfAllCircles)
                        {
                            foreach (RotatedRect rectangle in boxList)
                            {
                                if (Math.Sqrt(Math.Pow(rectangle.Center.X - circle.Center.X, 2) + Math.Pow(rectangle.Center.Y - circle.Center.Y, 2)) < 20)
                                { // the distance between the center of circle and rectangle
                                    NoEntrySign sign = new NoEntrySign();
                                    repo.AddNoEntrySign(sign);
                                }
                            }
                        }
                    }

                    #endregion

                    #region manage repo boolean for NO ENTRY sign
                    repo.SetBoolFoundNoEntrySign();  // setting true the bool of a found NO ENTRY sign if it is the case

                    if (repo.found_noEntry == true)
                    {
                        pictureBox_interzis.Visible = true;  
                    }
                    #endregion

                    #region draw circles
                    if (checkBox_HSV.Checked)
                    {
                        foreach (var circle in listOfAllCircles)
                        {
                            ImageFrame.Draw(circle, new Bgr(Color.Red), 5); // draw the circles
                        }

                    }

                    #endregion

                }
                finally
                {
                    channels[0].Dispose();
                    channels[1].Dispose();
                    channels[2].Dispose();
                    listOfAllCircles.Clear();
                    triangleList.Clear();
                    boxList.Clear();
                    linesList.Clear();
                }
            }








            // ----------------------------------------------------------- HAAR SPEED SIGNS-------------------------------------------------

            #region detection + recognition using HAAR
            if (checkBox_HaarCascadeForSpeedSigns.Checked)
            {
                 Console.WriteLine(".....  " + ImageFrame.Height + " " + ImageFrame.Width);  // = 480 x 640
                Image<Gray, byte> grayframe = ImageFrame.Convert<Gray, byte>(); // transform fame-ul in alb-negru pentru a putea fi prelucrat

                // take the values of parameters from the GUI
                MinNeighbors = tkBar_MinimumNeighbors.Value;
                MinNeighborsStop = tkBar_MinimumNeighborsSTOP.Value;
                WindowsSizeMin = tkBar_MinumumDetectionScale.Value;
                WindowsSizeMax = tkBar_MaximumDetectionScale.Value;
                ScaleIncreaseRate = Double.Parse(comboBoxScaleIncRate.Text);

                var stopSigns = haarStopSign.DetectMultiScale(   // apply Viola Jones algorithm to detect the speed signs
                       grayframe,   // the original frame transformed to gray image
                       ScaleIncreaseRate,  // how much the scale from Min to Max will increase while searching
                       MinNeighborsStop,  // how many neighbors need to be such that the sighn is a valid one
                       new Size(WindowsSizeMin, WindowsSizeMin),  // minimum size of the sign
                       new Size(WindowsSizeMax, WindowsSizeMax));  // maimum size of the sign
                foreach (var stop in stopSigns)
                {
                    ImageFrame.Draw(stop, new Bgr(Color.Yellow), 4);  // desenez stopul 
                    repo.AddStopSign(new StopSign());
                }

                // DETECTEZ SEMNELE
                var signs = haarSpeedSign.DetectMultiScale(   // apply Viola Jones algorithm to detect the speed signs
                    grayframe,   // the original frame transformed to gray image
                    ScaleIncreaseRate,  // how much the scale from Min to Max will increase while searching
                    MinNeighbors,  // how many neighbors need to be such that the sighn is a valid one
                    new Size(WindowsSizeMin, WindowsSizeMin),  // minimum size of the sign
                    new Size(WindowsSizeMax, WindowsSizeMax));  // maimum size of the sign

                // PARCURG SEMNELE
                foreach (var sign in signs) // pentru feicare semn gasit desenez dreptunghiul
                {
                    ImageFrame.Draw(sign, new Bgr(Color.Red), 2);  // desenez semnul 

                    SpeedLimitSign speedSign = new SpeedLimitSign(); // create object of type SpeedLimitSign

                    // SCHIMB ROI DOAR PE SEMNUL GASIT
                    var rect = new System.Drawing.Rectangle(sign.X, sign.Y, sign.Width, sign.Height); // setez dreptunghiul
                    ImageFrame.ROI = rect;   // aplic dreptunghiul ca fiind ROI

                    Image<Bgr, Byte> img = new Image<Bgr, Byte>(ImageFrame.ToBitmap());   // imaginea color a semnului


                    Image<Gray, byte> myImage = new Image<Gray, byte>(img.ToBitmap());  // imaginea Gray a semnului (pt a aplica Canny)
                    Gray cannyThreshold = new Gray(150);  // 180                                      
                    Gray circleAccumulatorThreshold = new Gray(60);  //120

                    CircleF[] circles = myImage.HoughCircles(
                        cannyThreshold,
                        circleAccumulatorThreshold,
                        0.2, //Resolution of the accumulator used to detect centers of the circles  era 5.0
                        20.0, //min distance  era 10.0
                        5, //min radius era 5
                        0 //max radius era 0
                        )[0]; //Get the circles from the first channel

                    speedSign.AllocatePossibleSpeeds(circles.Length);  // allocate memory for as many possible values of speed
                                                                       //as circles detected

                    pictureBox_ColoredCroppedSign.Image = img.ToBitmap();
                    CvInvoke.cvResetImageROI(ImageFrame);  // reset region of interest of the big original frame 


                    #region circles
                    foreach (var circle in circles)
                    {
                        img.Draw(circle, new Bgr(Color.Green), 2); // draw the circles
                        pictureBox_ColoredCroppedSign.Image = img.ToBitmap();
                        int radius = (int)circles[0].Radius;

                        int x = (int)circles[0].Center.X - radius;
                        int y = (int)circles[0].Center.Y - radius;
                        //Console.WriteLine("x: " + x + " y: " + y + " center: " + circles[0].Center + " radius: " + radius);   // stiu ca se refera la coordonatele din imaginea mica (imageBox4)


                        Bitmap tmp = new Bitmap(2 * radius, 2 * radius);

                        Graphics g = Graphics.FromImage(tmp);
                        g.TranslateTransform(tmp.Width / 2, tmp.Height / 2); // asa il aseaza bine in stanga sus

                        GraphicsPath path = new GraphicsPath();
                        path.AddEllipse(0 - radius, 0 - radius, 2 * radius, 2 * radius);
                        Region region = new Region(path);
                        g.SetClip(region, CombineMode.Replace);

                        Bitmap bmp = myImage.ToBitmap();
                       
                        g.DrawImage(bmp, new Rectangle(-radius * 2, -radius * 2, 4 * radius, 4 * radius), new Rectangle(x - radius + radius / 4, y - radius + radius / 4, 4 * radius - radius / 2, 4 * radius - radius / 2), GraphicsUnit.Pixel);

                        pictureBox_cropped_circle.Image = tmp;
                        tmp.Save("detected_circle/circle.jpg");


                        contorCreareOcr++;
                        if (contorCreareOcr == 6)
                        {

                            ocr = new OcrEngine();
                            contorCreareOcr = 0;
                        }

                        ocr.Image = ImageStream.FromFile("detected_circle/circle.jpg");
                        if (ocr.Process())
                        {
                            //Console.WriteLine("OCR:>>" + ocr.Text.ToString());
                            speedSign.AddPossibleSpeed(ocr.Text.ToString());
                        }


                    } // endforeach circles
                    #endregion end circles


                    repo.AddSpeedSign(speedSign);  // after i add all possible speeds as strings in the object, i put it in repo list
                } // end foreach(sign in signs)

                repo.setBoolFoundStopSign();
                repo.SetBoolsFoundSpeedSigns();   // set the booleans in repo for speed signs

                // based on booleans from repo, maxe picture boxes visible
                #region make the picture boxes visible
                if (repo.found_10 == true)
                {
                    pictureBox_limit_10.Visible = true;
                }
                if (repo.found_30 == true)
                {
                    pictureBox_limit_30.Visible = true;
                }
                if (repo.found_50 == true)
                {
                    pictureBox_limit_50.Visible = true;
                }
                if (repo.found_70 == true)
                {
                    pictureBox_limit_70.Visible = true;

                }
                if (repo.found_90 == true)
                {
                    pictureBox_limit_90.Visible = true;

                }
                if (repo.found_100 == true)
                {
                    pictureBox_limit_100.Visible = true;

                }
                if (repo.found_120 == true)
                {
                    pictureBox_limit_120.Visible = true;          
                }
                if (repo.found_130 == true)
                {
                    pictureBox_limit_130.Visible = true;             
                }
                if(repo.found_stop == true)
                {
                    pictureBox_stop.Visible = true;
                }

                #endregion



            }

            #endregion detect + recog using Haar


            // reset the repository for the next frame
            repo.ResetRepository();


            CamImageBox.Image = ImageFrame; // refrest the displayed frame   
           
        }
       






   

        // functia care face load la imagine si aplezeaza automat DetectSigns
        private void btnBrowse_Click(object sender, EventArgs e)
        {
           
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                Image InputImg = Image.FromFile(openFileDialog.FileName);
                ImageFrame = new Image<Bgr, byte>(new Bitmap(InputImg));

            
                CamImageBox.Image = ImageFrame;
                DetectSigns();
            }     
        }

        

        private void tkBar_MinimumNeighbors_Scroll(object sender, EventArgs e)
        {
            label_MinimumNeighbors.Text = tkBar_MinimumNeighbors.Value.ToString();
        }

        private void tkBar_MinumumDetectionScale_Scroll(object sender, EventArgs e)
        {
            label_MinimumDetectionScale.Text = tkBar_MinumumDetectionScale.Value.ToString();
            if (tkBar_MinumumDetectionScale.Value >= tkBar_MaximumDetectionScale.Value)
            {
                tkBar_MaximumDetectionScale.Value = tkBar_MinumumDetectionScale.Value + 1;
                label_MaximumDetectionScale.Text = tkBar_MaximumDetectionScale.Value.ToString();
            }
                
        }

        private void tkBar_MaximumDetectionScale_Scroll(object sender, EventArgs e)
        {
            label_MaximumDetectionScale.Text = tkBar_MaximumDetectionScale.Value.ToString();
            if(tkBar_MaximumDetectionScale.Value <= tkBar_MinumumDetectionScale.Value)
            {
                tkBar_MinumumDetectionScale.Value = tkBar_MaximumDetectionScale.Value - 1;
                label_MinimumDetectionScale.Text = tkBar_MinumumDetectionScale.Value.ToString();
            }
        }

        private void tkBar_MinimumHue_Scroll(object sender, EventArgs e)
        {
            label_MinHue.Text = tkBar_MinimumHue.Value.ToString();
            if(tkBar_MinimumHue.Value >= tkBar_MaximumHue.Value)
            {
                tkBar_MaximumHue.Value = tkBar_MinimumHue.Value + 1;
                label_MaxHue.Text = tkBar_MaximumHue.Value.ToString();
            }
        }

        private void tkBar_MaximumHue_Scroll(object sender, EventArgs e)
        {
            label_MaxHue.Text = tkBar_MaximumHue.Value.ToString();
            if (tkBar_MaximumHue.Value <= tkBar_MinimumHue.Value)
            {
                tkBar_MinimumHue.Value = tkBar_MaximumHue.Value - 1;
                label_MinHue.Text = tkBar_MinimumHue.Value.ToString();
            }
        }

        private void tkBar_MinimumSaturation_Scroll(object sender, EventArgs e)
        {
            label_MinSaturation.Text = tkBar_MinimumSaturation.Value.ToString();
            if (tkBar_MinimumSaturation.Value >= tkBar_MaximumSaturation.Value)
            {
                tkBar_MaximumSaturation.Value = tkBar_MinimumSaturation.Value + 1;
                label_MaxSaturation.Text = tkBar_MaximumSaturation.Value.ToString();
            }
        }

        private void tkBar_MaximumSaturation_Scroll(object sender, EventArgs e)
        {
            label_MaxSaturation.Text = tkBar_MaximumSaturation.Value.ToString();
            if (tkBar_MaximumSaturation.Value <= tkBar_MinimumSaturation.Value)
            {
                tkBar_MinimumSaturation.Value = tkBar_MaximumSaturation.Value - 1;
                label_MinSaturation.Text = tkBar_MinimumSaturation.Value.ToString();
            }
        }

        private void tkBar_MinimumValue_Scroll(object sender, EventArgs e)
        {
            label_MinValue.Text = tkBar_MinimumValue.Value.ToString();
            if (tkBar_MinimumValue.Value >= tkBar_MaximumValue.Value)
            {
                tkBar_MaximumValue.Value = tkBar_MinimumValue.Value + 1;
                label_MaxValue.Text = tkBar_MaximumValue.Value.ToString();
            }
        }

        private void tkBar_MaximumValue_Scroll(object sender, EventArgs e)
        {
            label_MaxValue.Text = tkBar_MaximumValue.Value.ToString();
            if (tkBar_MaximumValue.Value <= tkBar_MinimumValue.Value)
            {
                tkBar_MinimumValue.Value = tkBar_MaximumValue.Value - 1;
                label_MinValue.Text = tkBar_MinimumValue.Value.ToString();
            }
        }

        private void tkBar_CannyTreshold_Scroll(object sender, EventArgs e)
        {
            label_CannyTreshold.Text = tkBar_CannyTreshold.Value.ToString();
        }

        private void tkBar_CircleAccumulatorTreshold_Scroll(object sender, EventArgs e)
        {
            label_CircleAccumulatorTreshold.Text = tkBar_CircleAccumulatorTreshold.Value.ToString();
        }

        private void tkBar_RessAccCenter_Scroll(object sender, EventArgs e)
        {
            label_ReAccCenter.Text = tkBar_RessAccCenter.Value.ToString();
        }

        private void tkBar_MinDistanceBtwCenters_Scroll(object sender, EventArgs e)
        {
            label_MinDIstanceBtwCenters.Text = tkBar_MinDistanceBtwCenters.Value.ToString();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void tkBar_CannyMinThresholdLines_Scroll(object sender, EventArgs e)
        {
            label_CannyMinThresholdLines.Text = tkBar_CannyMinThresholdLines.Value.ToString();
        }

        private void tkBar_CannyThresholdLinkingLines_Scroll(object sender, EventArgs e)
        {
            label_CannyThresholdLinkingLines.Text = tkBar_CannyThresholdLinkingLines.Value.ToString();
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            label_ThresholdHoughLines.Text = tkBar_ThresholdHoughLines.Value.ToString();
        }

        private void tkBar_MinLineWidth_Scroll(object sender, EventArgs e)
        {
            label_MinLineWidth.Text = tkBar_MinLineLength.Value.ToString();
        }

        private void tkBar_GapBetweenLines_Scroll(object sender, EventArgs e)
        {
            label_GapBetweenLines.Text = tkBar_GapBetweenLines.Value.ToString();
        }

        private void tkBar_MinimumNeighborsSTOP_Scroll(object sender, EventArgs e)
        {
            label_MinimimNeighborsSTOP.Text = tkBar_MinimumNeighborsSTOP.Value.ToString();
        }
    }
}
