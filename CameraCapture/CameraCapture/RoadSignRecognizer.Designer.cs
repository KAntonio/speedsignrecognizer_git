﻿namespace RoadSignRecognizer
{
    partial class SpeedSignRecognizer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SpeedSignRecognizer));
            this.btnStart = new System.Windows.Forms.Button();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.labelTitle = new System.Windows.Forms.Label();
            this.labelScaleIncreaseRate = new System.Windows.Forms.Label();
            this.labelMinNeighbors = new System.Windows.Forms.Label();
            this.labelMinDetectionScale = new System.Windows.Forms.Label();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.comboBoxScaleIncRate = new System.Windows.Forms.ComboBox();
            this.labelMaxDetectionScale = new System.Windows.Forms.Label();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.tkBar_MinimumNeighbors = new System.Windows.Forms.TrackBar();
            this.label_MinimumNeighbors = new System.Windows.Forms.Label();
            this.tkBar_MinumumDetectionScale = new System.Windows.Forms.TrackBar();
            this.label_MinimumDetectionScale = new System.Windows.Forms.Label();
            this.label_MaximumDetectionScale = new System.Windows.Forms.Label();
            this.tkBar_MaximumDetectionScale = new System.Windows.Forms.TrackBar();
            this.pictureBox_limit_10 = new System.Windows.Forms.PictureBox();
            this.pictureBox_ColoredCroppedSign = new System.Windows.Forms.PictureBox();
            this.pictureBox_limit_130 = new System.Windows.Forms.PictureBox();
            this.pictureBox_limit_120 = new System.Windows.Forms.PictureBox();
            this.pictureBox_limit_30 = new System.Windows.Forms.PictureBox();
            this.pictureBox_limit_50 = new System.Windows.Forms.PictureBox();
            this.CamImageBox = new Emgu.CV.UI.ImageBox();
            this.pictureBox_cropped_circle = new System.Windows.Forms.PictureBox();
            this.pictureBox_Hue = new System.Windows.Forms.PictureBox();
            this.pictureBox_Saturation = new System.Windows.Forms.PictureBox();
            this.pictureBox_Value = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tkBar_MinimumHue = new System.Windows.Forms.TrackBar();
            this.tkBar_MaximumHue = new System.Windows.Forms.TrackBar();
            this.label_MinHue = new System.Windows.Forms.Label();
            this.label_MaxHue = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tkBar_MinimumSaturation = new System.Windows.Forms.TrackBar();
            this.tkBar_MaximumSaturation = new System.Windows.Forms.TrackBar();
            this.label_MinSaturation = new System.Windows.Forms.Label();
            this.label_MaxSaturation = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.tkBar_MinimumValue = new System.Windows.Forms.TrackBar();
            this.tkBar_MaximumValue = new System.Windows.Forms.TrackBar();
            this.label_MinValue = new System.Windows.Forms.Label();
            this.label_MaxValue = new System.Windows.Forms.Label();
            this.tkBar_CannyTreshold = new System.Windows.Forms.TrackBar();
            this.label10 = new System.Windows.Forms.Label();
            this.label_CannyTreshold = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label_CircleAccumulatorTreshold = new System.Windows.Forms.Label();
            this.tkBar_CircleAccumulatorTreshold = new System.Windows.Forms.TrackBar();
            this.label12 = new System.Windows.Forms.Label();
            this.tkBar_RessAccCenter = new System.Windows.Forms.TrackBar();
            this.label_ReAccCenter = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label_MinDIstanceBtwCenters = new System.Windows.Forms.Label();
            this.tkBar_MinDistanceBtwCenters = new System.Windows.Forms.TrackBar();
            this.checkBox_HSV = new System.Windows.Forms.CheckBox();
            this.pictureBox_limit_70 = new System.Windows.Forms.PictureBox();
            this.pictureBox_limit_90 = new System.Windows.Forms.PictureBox();
            this.pictureBox_limit_100 = new System.Windows.Forms.PictureBox();
            this.pictureBox_interzis = new System.Windows.Forms.PictureBox();
            this.pictureBox_interzis_ambele = new System.Windows.Forms.PictureBox();
            this.pictureBox_giveWay = new System.Windows.Forms.PictureBox();
            this.pictureBox_sfarsit_restrictii = new System.Windows.Forms.PictureBox();
            this.checkBox_lines = new System.Windows.Forms.CheckBox();
            this.checkBox_HaarCascadeForSpeedSigns = new System.Windows.Forms.CheckBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.tkBar_CannyMinThresholdLines = new System.Windows.Forms.TrackBar();
            this.tkBar_CannyThresholdLinkingLines = new System.Windows.Forms.TrackBar();
            this.label_CannyMinThresholdLines = new System.Windows.Forms.Label();
            this.label_CannyThresholdLinkingLines = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.tkBar_ThresholdHoughLines = new System.Windows.Forms.TrackBar();
            this.label_ThresholdHoughLines = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.tkBar_MinLineLength = new System.Windows.Forms.TrackBar();
            this.tkBar_GapBetweenLines = new System.Windows.Forms.TrackBar();
            this.label_MinLineWidth = new System.Windows.Forms.Label();
            this.label_GapBetweenLines = new System.Windows.Forms.Label();
            this.pictureBox_stop = new System.Windows.Forms.PictureBox();
            this.checkBox_RectanglesAndTriangles = new System.Windows.Forms.CheckBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label_MinimimNeighborsSTOP = new System.Windows.Forms.Label();
            this.tkBar_MinimumNeighborsSTOP = new System.Windows.Forms.TrackBar();
            ((System.ComponentModel.ISupportInitialize)(this.tkBar_MinimumNeighbors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tkBar_MinumumDetectionScale)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tkBar_MaximumDetectionScale)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_limit_10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_ColoredCroppedSign)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_limit_130)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_limit_120)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_limit_30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_limit_50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CamImageBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_cropped_circle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Hue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Saturation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Value)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tkBar_MinimumHue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tkBar_MaximumHue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tkBar_MinimumSaturation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tkBar_MaximumSaturation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tkBar_MinimumValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tkBar_MaximumValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tkBar_CannyTreshold)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tkBar_CircleAccumulatorTreshold)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tkBar_RessAccCenter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tkBar_MinDistanceBtwCenters)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_limit_70)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_limit_90)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_limit_100)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_interzis)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_interzis_ambele)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_giveWay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_sfarsit_restrictii)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tkBar_CannyMinThresholdLines)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tkBar_CannyThresholdLinkingLines)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tkBar_ThresholdHoughLines)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tkBar_MinLineLength)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tkBar_GapBetweenLines)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_stop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tkBar_MinimumNeighborsSTOP)).BeginInit();
            this.SuspendLayout();
            // 
            // btnStart
            // 
            this.btnStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStart.AutoEllipsis = true;
            this.btnStart.BackColor = System.Drawing.SystemColors.Window;
            this.btnStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStart.ForeColor = System.Drawing.Color.Black;
            this.btnStart.Location = new System.Drawing.Point(674, 605);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(170, 55);
            this.btnStart.TabIndex = 3;
            this.btnStart.Text = "Start  webcam!";
            this.btnStart.UseVisualStyleBackColor = false;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnBrowse
            // 
            this.btnBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBrowse.BackColor = System.Drawing.SystemColors.HighlightText;
            this.btnBrowse.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBrowse.ForeColor = System.Drawing.Color.Black;
            this.btnBrowse.Location = new System.Drawing.Point(674, 548);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(170, 51);
            this.btnBrowse.TabIndex = 4;
            this.btnBrowse.Text = "Load image";
            this.btnBrowse.UseVisualStyleBackColor = false;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // labelTitle
            // 
            this.labelTitle.AutoSize = true;
            this.labelTitle.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTitle.ForeColor = System.Drawing.Color.Red;
            this.labelTitle.Location = new System.Drawing.Point(889, 12);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(296, 18);
            this.labelTitle.TabIndex = 5;
            this.labelTitle.Text = "Viola-Jones speed signs detection parameters";
            // 
            // labelScaleIncreaseRate
            // 
            this.labelScaleIncreaseRate.AutoSize = true;
            this.labelScaleIncreaseRate.Location = new System.Drawing.Point(891, 52);
            this.labelScaleIncreaseRate.Name = "labelScaleIncreaseRate";
            this.labelScaleIncreaseRate.Size = new System.Drawing.Size(98, 13);
            this.labelScaleIncreaseRate.TabIndex = 6;
            this.labelScaleIncreaseRate.Text = "Scale increase rate";
            // 
            // labelMinNeighbors
            // 
            this.labelMinNeighbors.AutoSize = true;
            this.labelMinNeighbors.Location = new System.Drawing.Point(891, 79);
            this.labelMinNeighbors.Name = "labelMinNeighbors";
            this.labelMinNeighbors.Size = new System.Drawing.Size(67, 13);
            this.labelMinNeighbors.TabIndex = 7;
            this.labelMinNeighbors.Text = "Min neigh sp";
            // 
            // labelMinDetectionScale
            // 
            this.labelMinDetectionScale.AutoSize = true;
            this.labelMinDetectionScale.Location = new System.Drawing.Point(894, 163);
            this.labelMinDetectionScale.Name = "labelMinDetectionScale";
            this.labelMinDetectionScale.Size = new System.Drawing.Size(105, 13);
            this.labelMinDetectionScale.TabIndex = 8;
            this.labelMinDetectionScale.Text = "Min. detection scale ";
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog";
            // 
            // comboBoxScaleIncRate
            // 
            this.comboBoxScaleIncRate.FormattingEnabled = true;
            this.comboBoxScaleIncRate.Items.AddRange(new object[] {
            "1.1",
            "1.2",
            "1.3",
            "1.4"});
            this.comboBoxScaleIncRate.Location = new System.Drawing.Point(1004, 52);
            this.comboBoxScaleIncRate.Name = "comboBoxScaleIncRate";
            this.comboBoxScaleIncRate.Size = new System.Drawing.Size(121, 21);
            this.comboBoxScaleIncRate.TabIndex = 9;
            this.comboBoxScaleIncRate.Text = "1.1";
            // 
            // labelMaxDetectionScale
            // 
            this.labelMaxDetectionScale.AutoSize = true;
            this.labelMaxDetectionScale.Location = new System.Drawing.Point(894, 195);
            this.labelMaxDetectionScale.Name = "labelMaxDetectionScale";
            this.labelMaxDetectionScale.Size = new System.Drawing.Size(108, 13);
            this.labelMaxDetectionScale.TabIndex = 12;
            this.labelMaxDetectionScale.Text = "Max. detection scale ";
            // 
            // tkBar_MinimumNeighbors
            // 
            this.tkBar_MinimumNeighbors.Location = new System.Drawing.Point(1001, 79);
            this.tkBar_MinimumNeighbors.Maximum = 30;
            this.tkBar_MinimumNeighbors.Minimum = 1;
            this.tkBar_MinimumNeighbors.Name = "tkBar_MinimumNeighbors";
            this.tkBar_MinimumNeighbors.Size = new System.Drawing.Size(139, 45);
            this.tkBar_MinimumNeighbors.TabIndex = 19;
            this.tkBar_MinimumNeighbors.Tag = "";
            this.tkBar_MinimumNeighbors.Value = 6;
            this.tkBar_MinimumNeighbors.Scroll += new System.EventHandler(this.tkBar_MinimumNeighbors_Scroll);
            // 
            // label_MinimumNeighbors
            // 
            this.label_MinimumNeighbors.AutoSize = true;
            this.label_MinimumNeighbors.Location = new System.Drawing.Point(980, 79);
            this.label_MinimumNeighbors.Name = "label_MinimumNeighbors";
            this.label_MinimumNeighbors.Size = new System.Drawing.Size(13, 13);
            this.label_MinimumNeighbors.TabIndex = 20;
            this.label_MinimumNeighbors.Text = "6";
            // 
            // tkBar_MinumumDetectionScale
            // 
            this.tkBar_MinumumDetectionScale.Location = new System.Drawing.Point(1031, 154);
            this.tkBar_MinumumDetectionScale.Maximum = 400;
            this.tkBar_MinumumDetectionScale.Minimum = 1;
            this.tkBar_MinumumDetectionScale.Name = "tkBar_MinumumDetectionScale";
            this.tkBar_MinumumDetectionScale.Size = new System.Drawing.Size(104, 45);
            this.tkBar_MinumumDetectionScale.TabIndex = 21;
            this.tkBar_MinumumDetectionScale.Value = 50;
            this.tkBar_MinumumDetectionScale.Scroll += new System.EventHandler(this.tkBar_MinumumDetectionScale_Scroll);
            // 
            // label_MinimumDetectionScale
            // 
            this.label_MinimumDetectionScale.AutoSize = true;
            this.label_MinimumDetectionScale.Location = new System.Drawing.Point(1009, 163);
            this.label_MinimumDetectionScale.Name = "label_MinimumDetectionScale";
            this.label_MinimumDetectionScale.Size = new System.Drawing.Size(19, 13);
            this.label_MinimumDetectionScale.TabIndex = 22;
            this.label_MinimumDetectionScale.Text = "50";
            // 
            // label_MaximumDetectionScale
            // 
            this.label_MaximumDetectionScale.AutoSize = true;
            this.label_MaximumDetectionScale.Location = new System.Drawing.Point(1008, 198);
            this.label_MaximumDetectionScale.Name = "label_MaximumDetectionScale";
            this.label_MaximumDetectionScale.Size = new System.Drawing.Size(25, 13);
            this.label_MaximumDetectionScale.TabIndex = 23;
            this.label_MaximumDetectionScale.Text = "500";
            // 
            // tkBar_MaximumDetectionScale
            // 
            this.tkBar_MaximumDetectionScale.Location = new System.Drawing.Point(1038, 195);
            this.tkBar_MaximumDetectionScale.Maximum = 2000;
            this.tkBar_MaximumDetectionScale.Minimum = 10;
            this.tkBar_MaximumDetectionScale.Name = "tkBar_MaximumDetectionScale";
            this.tkBar_MaximumDetectionScale.Size = new System.Drawing.Size(104, 45);
            this.tkBar_MaximumDetectionScale.TabIndex = 24;
            this.tkBar_MaximumDetectionScale.Value = 500;
            this.tkBar_MaximumDetectionScale.Scroll += new System.EventHandler(this.tkBar_MaximumDetectionScale_Scroll);
            // 
            // pictureBox_limit_10
            // 
            this.pictureBox_limit_10.Image = global::CameraCapture.Properties.Resources.limita_10_imageBox;
            this.pictureBox_limit_10.Location = new System.Drawing.Point(599, 59);
            this.pictureBox_limit_10.Name = "pictureBox_limit_10";
            this.pictureBox_limit_10.Size = new System.Drawing.Size(42, 41);
            this.pictureBox_limit_10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_limit_10.TabIndex = 30;
            this.pictureBox_limit_10.TabStop = false;
            this.pictureBox_limit_10.Visible = false;
            // 
            // pictureBox_ColoredCroppedSign
            // 
            this.pictureBox_ColoredCroppedSign.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox_ColoredCroppedSign.Location = new System.Drawing.Point(587, 173);
            this.pictureBox_ColoredCroppedSign.Name = "pictureBox_ColoredCroppedSign";
            this.pictureBox_ColoredCroppedSign.Size = new System.Drawing.Size(128, 127);
            this.pictureBox_ColoredCroppedSign.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_ColoredCroppedSign.TabIndex = 29;
            this.pictureBox_ColoredCroppedSign.TabStop = false;
            // 
            // pictureBox_limit_130
            // 
            this.pictureBox_limit_130.Image = global::CameraCapture.Properties.Resources.limita_130_imageBox;
            this.pictureBox_limit_130.Location = new System.Drawing.Point(792, 12);
            this.pictureBox_limit_130.Name = "pictureBox_limit_130";
            this.pictureBox_limit_130.Size = new System.Drawing.Size(42, 41);
            this.pictureBox_limit_130.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_limit_130.TabIndex = 28;
            this.pictureBox_limit_130.TabStop = false;
            this.pictureBox_limit_130.Visible = false;
            // 
            // pictureBox_limit_120
            // 
            this.pictureBox_limit_120.Image = global::CameraCapture.Properties.Resources.limita_120_imageBox;
            this.pictureBox_limit_120.Location = new System.Drawing.Point(744, 12);
            this.pictureBox_limit_120.Name = "pictureBox_limit_120";
            this.pictureBox_limit_120.Size = new System.Drawing.Size(42, 41);
            this.pictureBox_limit_120.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_limit_120.TabIndex = 27;
            this.pictureBox_limit_120.TabStop = false;
            this.pictureBox_limit_120.Visible = false;
            // 
            // pictureBox_limit_30
            // 
            this.pictureBox_limit_30.Image = global::CameraCapture.Properties.Resources.limita_30_imageBox;
            this.pictureBox_limit_30.Location = new System.Drawing.Point(647, 59);
            this.pictureBox_limit_30.Name = "pictureBox_limit_30";
            this.pictureBox_limit_30.Size = new System.Drawing.Size(42, 41);
            this.pictureBox_limit_30.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_limit_30.TabIndex = 26;
            this.pictureBox_limit_30.TabStop = false;
            this.pictureBox_limit_30.Visible = false;
            // 
            // pictureBox_limit_50
            // 
            this.pictureBox_limit_50.Image = global::CameraCapture.Properties.Resources.limita_50_imageBox;
            this.pictureBox_limit_50.Location = new System.Drawing.Point(695, 59);
            this.pictureBox_limit_50.Name = "pictureBox_limit_50";
            this.pictureBox_limit_50.Size = new System.Drawing.Size(42, 41);
            this.pictureBox_limit_50.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_limit_50.TabIndex = 25;
            this.pictureBox_limit_50.TabStop = false;
            this.pictureBox_limit_50.Visible = false;
            // 
            // CamImageBox
            // 
            this.CamImageBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.CamImageBox.Location = new System.Drawing.Point(12, 12);
            this.CamImageBox.Name = "CamImageBox";
            this.CamImageBox.Size = new System.Drawing.Size(569, 421);
            this.CamImageBox.TabIndex = 2;
            this.CamImageBox.TabStop = false;
            // 
            // pictureBox_cropped_circle
            // 
            this.pictureBox_cropped_circle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox_cropped_circle.Location = new System.Drawing.Point(721, 173);
            this.pictureBox_cropped_circle.Name = "pictureBox_cropped_circle";
            this.pictureBox_cropped_circle.Size = new System.Drawing.Size(123, 127);
            this.pictureBox_cropped_circle.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_cropped_circle.TabIndex = 31;
            this.pictureBox_cropped_circle.TabStop = false;
            // 
            // pictureBox_Hue
            // 
            this.pictureBox_Hue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBox_Hue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox_Hue.Location = new System.Drawing.Point(12, 460);
            this.pictureBox_Hue.Name = "pictureBox_Hue";
            this.pictureBox_Hue.Size = new System.Drawing.Size(152, 97);
            this.pictureBox_Hue.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_Hue.TabIndex = 32;
            this.pictureBox_Hue.TabStop = false;
            // 
            // pictureBox_Saturation
            // 
            this.pictureBox_Saturation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBox_Saturation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox_Saturation.Location = new System.Drawing.Point(227, 460);
            this.pictureBox_Saturation.Name = "pictureBox_Saturation";
            this.pictureBox_Saturation.Size = new System.Drawing.Size(152, 97);
            this.pictureBox_Saturation.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_Saturation.TabIndex = 33;
            this.pictureBox_Saturation.TabStop = false;
            // 
            // pictureBox_Value
            // 
            this.pictureBox_Value.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBox_Value.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox_Value.Location = new System.Drawing.Point(429, 460);
            this.pictureBox_Value.Name = "pictureBox_Value";
            this.pictureBox_Value.Size = new System.Drawing.Size(152, 97);
            this.pictureBox_Value.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_Value.TabIndex = 34;
            this.pictureBox_Value.TabStop = false;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(264, 568);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 22);
            this.label1.TabIndex = 36;
            this.label1.Text = "Saturation";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(55, 566);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 22);
            this.label2.TabIndex = 37;
            this.label2.Text = "Hue";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(480, 567);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 22);
            this.label3.TabIndex = 38;
            this.label3.Text = "Value";
            // 
            // tkBar_MinimumHue
            // 
            this.tkBar_MinimumHue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tkBar_MinimumHue.Location = new System.Drawing.Point(41, 593);
            this.tkBar_MinimumHue.Maximum = 179;
            this.tkBar_MinimumHue.Name = "tkBar_MinimumHue";
            this.tkBar_MinimumHue.Size = new System.Drawing.Size(104, 45);
            this.tkBar_MinimumHue.TabIndex = 39;
            this.tkBar_MinimumHue.Value = 165;
            this.tkBar_MinimumHue.Scroll += new System.EventHandler(this.tkBar_MinimumHue_Scroll);
            // 
            // tkBar_MaximumHue
            // 
            this.tkBar_MaximumHue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tkBar_MaximumHue.Location = new System.Drawing.Point(41, 627);
            this.tkBar_MaximumHue.Maximum = 180;
            this.tkBar_MaximumHue.Minimum = 1;
            this.tkBar_MaximumHue.Name = "tkBar_MaximumHue";
            this.tkBar_MaximumHue.Size = new System.Drawing.Size(104, 45);
            this.tkBar_MaximumHue.TabIndex = 40;
            this.tkBar_MaximumHue.Value = 180;
            this.tkBar_MaximumHue.Scroll += new System.EventHandler(this.tkBar_MaximumHue_Scroll);
            // 
            // label_MinHue
            // 
            this.label_MinHue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label_MinHue.AutoSize = true;
            this.label_MinHue.Location = new System.Drawing.Point(151, 595);
            this.label_MinHue.Name = "label_MinHue";
            this.label_MinHue.Size = new System.Drawing.Size(25, 13);
            this.label_MinHue.TabIndex = 41;
            this.label_MinHue.Text = "165";
            // 
            // label_MaxHue
            // 
            this.label_MaxHue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label_MaxHue.AutoSize = true;
            this.label_MaxHue.Location = new System.Drawing.Point(151, 632);
            this.label_MaxHue.Name = "label_MaxHue";
            this.label_MaxHue.Size = new System.Drawing.Size(25, 13);
            this.label_MaxHue.TabIndex = 42;
            this.label_MaxHue.Text = "180";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 599);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(24, 13);
            this.label4.TabIndex = 43;
            this.label4.Text = "Min";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(17, 633);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(27, 13);
            this.label5.TabIndex = 44;
            this.label5.Text = "Max";
            // 
            // tkBar_MinimumSaturation
            // 
            this.tkBar_MinimumSaturation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tkBar_MinimumSaturation.Location = new System.Drawing.Point(237, 593);
            this.tkBar_MinimumSaturation.Maximum = 254;
            this.tkBar_MinimumSaturation.Name = "tkBar_MinimumSaturation";
            this.tkBar_MinimumSaturation.Size = new System.Drawing.Size(104, 45);
            this.tkBar_MinimumSaturation.TabIndex = 45;
            this.tkBar_MinimumSaturation.Value = 82;
            this.tkBar_MinimumSaturation.Scroll += new System.EventHandler(this.tkBar_MinimumSaturation_Scroll);
            // 
            // tkBar_MaximumSaturation
            // 
            this.tkBar_MaximumSaturation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tkBar_MaximumSaturation.Location = new System.Drawing.Point(237, 627);
            this.tkBar_MaximumSaturation.Maximum = 255;
            this.tkBar_MaximumSaturation.Minimum = 1;
            this.tkBar_MaximumSaturation.Name = "tkBar_MaximumSaturation";
            this.tkBar_MaximumSaturation.Size = new System.Drawing.Size(104, 45);
            this.tkBar_MaximumSaturation.TabIndex = 46;
            this.tkBar_MaximumSaturation.Value = 250;
            this.tkBar_MaximumSaturation.Scroll += new System.EventHandler(this.tkBar_MaximumSaturation_Scroll);
            // 
            // label_MinSaturation
            // 
            this.label_MinSaturation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label_MinSaturation.AutoSize = true;
            this.label_MinSaturation.Location = new System.Drawing.Point(347, 602);
            this.label_MinSaturation.Name = "label_MinSaturation";
            this.label_MinSaturation.Size = new System.Drawing.Size(19, 13);
            this.label_MinSaturation.TabIndex = 47;
            this.label_MinSaturation.Text = "82";
            // 
            // label_MaxSaturation
            // 
            this.label_MaxSaturation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label_MaxSaturation.AutoSize = true;
            this.label_MaxSaturation.Location = new System.Drawing.Point(347, 634);
            this.label_MaxSaturation.Name = "label_MaxSaturation";
            this.label_MaxSaturation.Size = new System.Drawing.Size(25, 13);
            this.label_MaxSaturation.TabIndex = 48;
            this.label_MaxSaturation.Text = "250";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(211, 598);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(27, 13);
            this.label6.TabIndex = 49;
            this.label6.Text = "Min ";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(212, 637);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(30, 13);
            this.label7.TabIndex = 50;
            this.label7.Text = "Max ";
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(429, 602);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(24, 13);
            this.label8.TabIndex = 51;
            this.label8.Text = "Min";
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(429, 639);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(27, 13);
            this.label9.TabIndex = 52;
            this.label9.Text = "Max";
            // 
            // tkBar_MinimumValue
            // 
            this.tkBar_MinimumValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tkBar_MinimumValue.Location = new System.Drawing.Point(460, 594);
            this.tkBar_MinimumValue.Maximum = 254;
            this.tkBar_MinimumValue.Name = "tkBar_MinimumValue";
            this.tkBar_MinimumValue.Size = new System.Drawing.Size(104, 45);
            this.tkBar_MinimumValue.TabIndex = 53;
            this.tkBar_MinimumValue.Value = 227;
            this.tkBar_MinimumValue.Scroll += new System.EventHandler(this.tkBar_MinimumValue_Scroll);
            // 
            // tkBar_MaximumValue
            // 
            this.tkBar_MaximumValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tkBar_MaximumValue.Location = new System.Drawing.Point(462, 627);
            this.tkBar_MaximumValue.Maximum = 255;
            this.tkBar_MaximumValue.Minimum = 1;
            this.tkBar_MaximumValue.Name = "tkBar_MaximumValue";
            this.tkBar_MaximumValue.Size = new System.Drawing.Size(104, 45);
            this.tkBar_MaximumValue.TabIndex = 54;
            this.tkBar_MaximumValue.Value = 255;
            this.tkBar_MaximumValue.Scroll += new System.EventHandler(this.tkBar_MaximumValue_Scroll);
            // 
            // label_MinValue
            // 
            this.label_MinValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label_MinValue.AutoSize = true;
            this.label_MinValue.Location = new System.Drawing.Point(567, 602);
            this.label_MinValue.Name = "label_MinValue";
            this.label_MinValue.Size = new System.Drawing.Size(25, 13);
            this.label_MinValue.TabIndex = 55;
            this.label_MinValue.Text = "227";
            // 
            // label_MaxValue
            // 
            this.label_MaxValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label_MaxValue.AutoSize = true;
            this.label_MaxValue.Location = new System.Drawing.Point(567, 639);
            this.label_MaxValue.Name = "label_MaxValue";
            this.label_MaxValue.Size = new System.Drawing.Size(25, 13);
            this.label_MaxValue.TabIndex = 56;
            this.label_MaxValue.Text = "255";
            // 
            // tkBar_CannyTreshold
            // 
            this.tkBar_CannyTreshold.Location = new System.Drawing.Point(1027, 276);
            this.tkBar_CannyTreshold.Maximum = 300;
            this.tkBar_CannyTreshold.Minimum = 5;
            this.tkBar_CannyTreshold.Name = "tkBar_CannyTreshold";
            this.tkBar_CannyTreshold.Size = new System.Drawing.Size(104, 45);
            this.tkBar_CannyTreshold.TabIndex = 57;
            this.tkBar_CannyTreshold.Value = 100;
            this.tkBar_CannyTreshold.Scroll += new System.EventHandler(this.tkBar_CannyTreshold_Scroll);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(893, 276);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(83, 13);
            this.label10.TabIndex = 58;
            this.label10.Text = "Canny threshold";
            // 
            // label_CannyTreshold
            // 
            this.label_CannyTreshold.AutoSize = true;
            this.label_CannyTreshold.Location = new System.Drawing.Point(1137, 284);
            this.label_CannyTreshold.Name = "label_CannyTreshold";
            this.label_CannyTreshold.Size = new System.Drawing.Size(25, 13);
            this.label_CannyTreshold.TabIndex = 59;
            this.label_CannyTreshold.Text = "100";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(893, 318);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(141, 13);
            this.label11.TabIndex = 60;
            this.label11.Text = "CIrcle accumulator threshold";
            // 
            // label_CircleAccumulatorTreshold
            // 
            this.label_CircleAccumulatorTreshold.AutoSize = true;
            this.label_CircleAccumulatorTreshold.Location = new System.Drawing.Point(1143, 318);
            this.label_CircleAccumulatorTreshold.Name = "label_CircleAccumulatorTreshold";
            this.label_CircleAccumulatorTreshold.Size = new System.Drawing.Size(19, 13);
            this.label_CircleAccumulatorTreshold.TabIndex = 61;
            this.label_CircleAccumulatorTreshold.Text = "70";
            // 
            // tkBar_CircleAccumulatorTreshold
            // 
            this.tkBar_CircleAccumulatorTreshold.Location = new System.Drawing.Point(1038, 318);
            this.tkBar_CircleAccumulatorTreshold.Maximum = 200;
            this.tkBar_CircleAccumulatorTreshold.Minimum = 4;
            this.tkBar_CircleAccumulatorTreshold.Name = "tkBar_CircleAccumulatorTreshold";
            this.tkBar_CircleAccumulatorTreshold.Size = new System.Drawing.Size(104, 45);
            this.tkBar_CircleAccumulatorTreshold.TabIndex = 62;
            this.tkBar_CircleAccumulatorTreshold.Value = 70;
            this.tkBar_CircleAccumulatorTreshold.Scroll += new System.EventHandler(this.tkBar_CircleAccumulatorTreshold_Scroll);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(894, 357);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(80, 13);
            this.label12.TabIndex = 63;
            this.label12.Text = "Res acc center";
            // 
            // tkBar_RessAccCenter
            // 
            this.tkBar_RessAccCenter.Location = new System.Drawing.Point(1027, 359);
            this.tkBar_RessAccCenter.Maximum = 100;
            this.tkBar_RessAccCenter.Minimum = 1;
            this.tkBar_RessAccCenter.Name = "tkBar_RessAccCenter";
            this.tkBar_RessAccCenter.Size = new System.Drawing.Size(104, 45);
            this.tkBar_RessAccCenter.TabIndex = 64;
            this.tkBar_RessAccCenter.Value = 14;
            this.tkBar_RessAccCenter.Scroll += new System.EventHandler(this.tkBar_RessAccCenter_Scroll);
            // 
            // label_ReAccCenter
            // 
            this.label_ReAccCenter.AutoSize = true;
            this.label_ReAccCenter.Location = new System.Drawing.Point(1137, 367);
            this.label_ReAccCenter.Name = "label_ReAccCenter";
            this.label_ReAccCenter.Size = new System.Drawing.Size(19, 13);
            this.label_ReAccCenter.TabIndex = 65;
            this.label_ReAccCenter.Text = "14";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(894, 396);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(128, 13);
            this.label13.TabIndex = 66;
            this.label13.Text = "Min distance btw. centers";
            // 
            // label_MinDIstanceBtwCenters
            // 
            this.label_MinDIstanceBtwCenters.AutoSize = true;
            this.label_MinDIstanceBtwCenters.Location = new System.Drawing.Point(1143, 406);
            this.label_MinDIstanceBtwCenters.Name = "label_MinDIstanceBtwCenters";
            this.label_MinDIstanceBtwCenters.Size = new System.Drawing.Size(19, 13);
            this.label_MinDIstanceBtwCenters.TabIndex = 67;
            this.label_MinDIstanceBtwCenters.Text = "50";
            // 
            // tkBar_MinDistanceBtwCenters
            // 
            this.tkBar_MinDistanceBtwCenters.Location = new System.Drawing.Point(1027, 396);
            this.tkBar_MinDistanceBtwCenters.Maximum = 1000;
            this.tkBar_MinDistanceBtwCenters.Minimum = 1;
            this.tkBar_MinDistanceBtwCenters.Name = "tkBar_MinDistanceBtwCenters";
            this.tkBar_MinDistanceBtwCenters.Size = new System.Drawing.Size(104, 45);
            this.tkBar_MinDistanceBtwCenters.TabIndex = 68;
            this.tkBar_MinDistanceBtwCenters.Value = 50;
            this.tkBar_MinDistanceBtwCenters.Scroll += new System.EventHandler(this.tkBar_MinDistanceBtwCenters_Scroll);
            // 
            // checkBox_HSV
            // 
            this.checkBox_HSV.AutoSize = true;
            this.checkBox_HSV.Location = new System.Drawing.Point(588, 326);
            this.checkBox_HSV.Name = "checkBox_HSV";
            this.checkBox_HSV.Size = new System.Drawing.Size(84, 17);
            this.checkBox_HSV.TabIndex = 69;
            this.checkBox_HSV.Text = "Draw circles";
            this.checkBox_HSV.UseVisualStyleBackColor = true;
            this.checkBox_HSV.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // pictureBox_limit_70
            // 
            this.pictureBox_limit_70.Image = global::CameraCapture.Properties.Resources.limita_70_imageBox;
            this.pictureBox_limit_70.Location = new System.Drawing.Point(597, 12);
            this.pictureBox_limit_70.Name = "pictureBox_limit_70";
            this.pictureBox_limit_70.Size = new System.Drawing.Size(43, 41);
            this.pictureBox_limit_70.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_limit_70.TabIndex = 70;
            this.pictureBox_limit_70.TabStop = false;
            this.pictureBox_limit_70.Visible = false;
            // 
            // pictureBox_limit_90
            // 
            this.pictureBox_limit_90.Image = global::CameraCapture.Properties.Resources.limita_90_imageBox;
            this.pictureBox_limit_90.Location = new System.Drawing.Point(646, 12);
            this.pictureBox_limit_90.Name = "pictureBox_limit_90";
            this.pictureBox_limit_90.Size = new System.Drawing.Size(43, 41);
            this.pictureBox_limit_90.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_limit_90.TabIndex = 71;
            this.pictureBox_limit_90.TabStop = false;
            this.pictureBox_limit_90.Visible = false;
            // 
            // pictureBox_limit_100
            // 
            this.pictureBox_limit_100.Image = global::CameraCapture.Properties.Resources.limita_100_imageBox;
            this.pictureBox_limit_100.Location = new System.Drawing.Point(695, 12);
            this.pictureBox_limit_100.Name = "pictureBox_limit_100";
            this.pictureBox_limit_100.Size = new System.Drawing.Size(43, 41);
            this.pictureBox_limit_100.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_limit_100.TabIndex = 72;
            this.pictureBox_limit_100.TabStop = false;
            this.pictureBox_limit_100.Visible = false;
            // 
            // pictureBox_interzis
            // 
            this.pictureBox_interzis.Image = global::CameraCapture.Properties.Resources.interzis;
            this.pictureBox_interzis.Location = new System.Drawing.Point(744, 59);
            this.pictureBox_interzis.Name = "pictureBox_interzis";
            this.pictureBox_interzis.Size = new System.Drawing.Size(42, 41);
            this.pictureBox_interzis.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_interzis.TabIndex = 73;
            this.pictureBox_interzis.TabStop = false;
            this.pictureBox_interzis.Visible = false;
            // 
            // pictureBox_interzis_ambele
            // 
            this.pictureBox_interzis_ambele.Image = global::CameraCapture.Properties.Resources.interzis_ambele;
            this.pictureBox_interzis_ambele.Location = new System.Drawing.Point(695, 106);
            this.pictureBox_interzis_ambele.Name = "pictureBox_interzis_ambele";
            this.pictureBox_interzis_ambele.Size = new System.Drawing.Size(42, 41);
            this.pictureBox_interzis_ambele.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_interzis_ambele.TabIndex = 74;
            this.pictureBox_interzis_ambele.TabStop = false;
            this.pictureBox_interzis_ambele.Visible = false;
            // 
            // pictureBox_giveWay
            // 
            this.pictureBox_giveWay.Image = global::CameraCapture.Properties.Resources.cedeaza;
            this.pictureBox_giveWay.Location = new System.Drawing.Point(792, 59);
            this.pictureBox_giveWay.Name = "pictureBox_giveWay";
            this.pictureBox_giveWay.Size = new System.Drawing.Size(42, 41);
            this.pictureBox_giveWay.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_giveWay.TabIndex = 75;
            this.pictureBox_giveWay.TabStop = false;
            this.pictureBox_giveWay.Visible = false;
            // 
            // pictureBox_sfarsit_restrictii
            // 
            this.pictureBox_sfarsit_restrictii.Image = global::CameraCapture.Properties.Resources.sfarsit_restrictie;
            this.pictureBox_sfarsit_restrictii.Location = new System.Drawing.Point(648, 106);
            this.pictureBox_sfarsit_restrictii.Name = "pictureBox_sfarsit_restrictii";
            this.pictureBox_sfarsit_restrictii.Size = new System.Drawing.Size(42, 41);
            this.pictureBox_sfarsit_restrictii.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_sfarsit_restrictii.TabIndex = 76;
            this.pictureBox_sfarsit_restrictii.TabStop = false;
            this.pictureBox_sfarsit_restrictii.Visible = false;
            // 
            // checkBox_lines
            // 
            this.checkBox_lines.AutoSize = true;
            this.checkBox_lines.Location = new System.Drawing.Point(588, 350);
            this.checkBox_lines.Name = "checkBox_lines";
            this.checkBox_lines.Size = new System.Drawing.Size(75, 17);
            this.checkBox_lines.TabIndex = 77;
            this.checkBox_lines.Text = "Draw lines";
            this.checkBox_lines.UseVisualStyleBackColor = true;
            // 
            // checkBox_HaarCascadeForSpeedSigns
            // 
            this.checkBox_HaarCascadeForSpeedSigns.AutoSize = true;
            this.checkBox_HaarCascadeForSpeedSigns.Checked = true;
            this.checkBox_HaarCascadeForSpeedSigns.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_HaarCascadeForSpeedSigns.Location = new System.Drawing.Point(587, 398);
            this.checkBox_HaarCascadeForSpeedSigns.Name = "checkBox_HaarCascadeForSpeedSigns";
            this.checkBox_HaarCascadeForSpeedSigns.Size = new System.Drawing.Size(260, 17);
            this.checkBox_HaarCascadeForSpeedSigns.TabIndex = 78;
            this.checkBox_HaarCascadeForSpeedSigns.Text = "Detect speed and stop signs using Haar Cascade";
            this.checkBox_HaarCascadeForSpeedSigns.UseVisualStyleBackColor = true;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Location = new System.Drawing.Point(891, 244);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(187, 18);
            this.label14.TabIndex = 79;
            this.label14.Text = "Circles detection parameters";
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Red;
            this.label15.Location = new System.Drawing.Point(889, 442);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(175, 18);
            this.label15.TabIndex = 80;
            this.label15.Text = "Line detection parameters";
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(891, 485);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(102, 13);
            this.label16.TabIndex = 81;
            this.label16.Text = "Canny min threshold";
            // 
            // label17
            // 
            this.label17.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(891, 518);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(116, 13);
            this.label17.TabIndex = 82;
            this.label17.Text = "Canny threshold linking";
            // 
            // tkBar_CannyMinThresholdLines
            // 
            this.tkBar_CannyMinThresholdLines.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tkBar_CannyMinThresholdLines.Location = new System.Drawing.Point(996, 476);
            this.tkBar_CannyMinThresholdLines.Maximum = 200;
            this.tkBar_CannyMinThresholdLines.Minimum = 10;
            this.tkBar_CannyMinThresholdLines.Name = "tkBar_CannyMinThresholdLines";
            this.tkBar_CannyMinThresholdLines.Size = new System.Drawing.Size(104, 45);
            this.tkBar_CannyMinThresholdLines.TabIndex = 83;
            this.tkBar_CannyMinThresholdLines.Value = 100;
            this.tkBar_CannyMinThresholdLines.Scroll += new System.EventHandler(this.tkBar_CannyMinThresholdLines_Scroll);
            // 
            // tkBar_CannyThresholdLinkingLines
            // 
            this.tkBar_CannyThresholdLinkingLines.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tkBar_CannyThresholdLinkingLines.Location = new System.Drawing.Point(1013, 517);
            this.tkBar_CannyThresholdLinkingLines.Maximum = 250;
            this.tkBar_CannyThresholdLinkingLines.Minimum = 10;
            this.tkBar_CannyThresholdLinkingLines.Name = "tkBar_CannyThresholdLinkingLines";
            this.tkBar_CannyThresholdLinkingLines.Size = new System.Drawing.Size(104, 45);
            this.tkBar_CannyThresholdLinkingLines.TabIndex = 84;
            this.tkBar_CannyThresholdLinkingLines.Value = 200;
            this.tkBar_CannyThresholdLinkingLines.Scroll += new System.EventHandler(this.tkBar_CannyThresholdLinkingLines_Scroll);
            // 
            // label_CannyMinThresholdLines
            // 
            this.label_CannyMinThresholdLines.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label_CannyMinThresholdLines.AutoSize = true;
            this.label_CannyMinThresholdLines.Location = new System.Drawing.Point(1108, 485);
            this.label_CannyMinThresholdLines.Name = "label_CannyMinThresholdLines";
            this.label_CannyMinThresholdLines.Size = new System.Drawing.Size(25, 13);
            this.label_CannyMinThresholdLines.TabIndex = 85;
            this.label_CannyMinThresholdLines.Text = "100";
            // 
            // label_CannyThresholdLinkingLines
            // 
            this.label_CannyThresholdLinkingLines.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label_CannyThresholdLinkingLines.AutoSize = true;
            this.label_CannyThresholdLinkingLines.Location = new System.Drawing.Point(1123, 518);
            this.label_CannyThresholdLinkingLines.Name = "label_CannyThresholdLinkingLines";
            this.label_CannyThresholdLinkingLines.Size = new System.Drawing.Size(25, 13);
            this.label_CannyThresholdLinkingLines.TabIndex = 86;
            this.label_CannyThresholdLinkingLines.Text = "200";
            // 
            // label18
            // 
            this.label18.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(891, 549);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(113, 13);
            this.label18.TabIndex = 87;
            this.label18.Text = "Threshold Hough lines";
            // 
            // tkBar_ThresholdHoughLines
            // 
            this.tkBar_ThresholdHoughLines.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tkBar_ThresholdHoughLines.Location = new System.Drawing.Point(1021, 549);
            this.tkBar_ThresholdHoughLines.Maximum = 200;
            this.tkBar_ThresholdHoughLines.Minimum = 5;
            this.tkBar_ThresholdHoughLines.Name = "tkBar_ThresholdHoughLines";
            this.tkBar_ThresholdHoughLines.Size = new System.Drawing.Size(104, 45);
            this.tkBar_ThresholdHoughLines.TabIndex = 88;
            this.tkBar_ThresholdHoughLines.Value = 100;
            this.tkBar_ThresholdHoughLines.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            // 
            // label_ThresholdHoughLines
            // 
            this.label_ThresholdHoughLines.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label_ThresholdHoughLines.AutoSize = true;
            this.label_ThresholdHoughLines.Location = new System.Drawing.Point(1131, 549);
            this.label_ThresholdHoughLines.Name = "label_ThresholdHoughLines";
            this.label_ThresholdHoughLines.Size = new System.Drawing.Size(25, 13);
            this.label_ThresholdHoughLines.TabIndex = 89;
            this.label_ThresholdHoughLines.Text = "100";
            // 
            // label19
            // 
            this.label19.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(892, 586);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(75, 13);
            this.label19.TabIndex = 90;
            this.label19.Text = "Min line length";
            // 
            // label20
            // 
            this.label20.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(892, 621);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(74, 13);
            this.label20.TabIndex = 91;
            this.label20.Text = "Gap btw. lines";
            // 
            // tkBar_MinLineLength
            // 
            this.tkBar_MinLineLength.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tkBar_MinLineLength.Location = new System.Drawing.Point(975, 586);
            this.tkBar_MinLineLength.Maximum = 100;
            this.tkBar_MinLineLength.Minimum = 1;
            this.tkBar_MinLineLength.Name = "tkBar_MinLineLength";
            this.tkBar_MinLineLength.Size = new System.Drawing.Size(104, 45);
            this.tkBar_MinLineLength.TabIndex = 92;
            this.tkBar_MinLineLength.Value = 30;
            this.tkBar_MinLineLength.Scroll += new System.EventHandler(this.tkBar_MinLineWidth_Scroll);
            // 
            // tkBar_GapBetweenLines
            // 
            this.tkBar_GapBetweenLines.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tkBar_GapBetweenLines.Location = new System.Drawing.Point(975, 621);
            this.tkBar_GapBetweenLines.Maximum = 100;
            this.tkBar_GapBetweenLines.Minimum = 1;
            this.tkBar_GapBetweenLines.Name = "tkBar_GapBetweenLines";
            this.tkBar_GapBetweenLines.Size = new System.Drawing.Size(104, 45);
            this.tkBar_GapBetweenLines.TabIndex = 93;
            this.tkBar_GapBetweenLines.Value = 10;
            this.tkBar_GapBetweenLines.Scroll += new System.EventHandler(this.tkBar_GapBetweenLines_Scroll);
            // 
            // label_MinLineWidth
            // 
            this.label_MinLineWidth.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label_MinLineWidth.AutoSize = true;
            this.label_MinLineWidth.Location = new System.Drawing.Point(1085, 592);
            this.label_MinLineWidth.Name = "label_MinLineWidth";
            this.label_MinLineWidth.Size = new System.Drawing.Size(19, 13);
            this.label_MinLineWidth.TabIndex = 94;
            this.label_MinLineWidth.Text = "30";
            // 
            // label_GapBetweenLines
            // 
            this.label_GapBetweenLines.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label_GapBetweenLines.AutoSize = true;
            this.label_GapBetweenLines.Location = new System.Drawing.Point(1074, 621);
            this.label_GapBetweenLines.Name = "label_GapBetweenLines";
            this.label_GapBetweenLines.Size = new System.Drawing.Size(19, 13);
            this.label_GapBetweenLines.TabIndex = 95;
            this.label_GapBetweenLines.Text = "10";
            // 
            // pictureBox_stop
            // 
            this.pictureBox_stop.Image = global::CameraCapture.Properties.Resources.stop_imageBox;
            this.pictureBox_stop.Location = new System.Drawing.Point(791, 106);
            this.pictureBox_stop.Name = "pictureBox_stop";
            this.pictureBox_stop.Size = new System.Drawing.Size(43, 41);
            this.pictureBox_stop.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_stop.TabIndex = 96;
            this.pictureBox_stop.TabStop = false;
            this.pictureBox_stop.Visible = false;
            // 
            // checkBox_RectanglesAndTriangles
            // 
            this.checkBox_RectanglesAndTriangles.AutoSize = true;
            this.checkBox_RectanglesAndTriangles.Checked = true;
            this.checkBox_RectanglesAndTriangles.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_RectanglesAndTriangles.Location = new System.Drawing.Point(587, 375);
            this.checkBox_RectanglesAndTriangles.Name = "checkBox_RectanglesAndTriangles";
            this.checkBox_RectanglesAndTriangles.Size = new System.Drawing.Size(166, 17);
            this.checkBox_RectanglesAndTriangles.TabIndex = 97;
            this.checkBox_RectanglesAndTriangles.Text = "Draw triangles and rectangles";
            this.checkBox_RectanglesAndTriangles.UseVisualStyleBackColor = true;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(894, 123);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(64, 13);
            this.label21.TabIndex = 98;
            this.label21.Text = "Min neigh st";
            // 
            // label_MinimimNeighborsSTOP
            // 
            this.label_MinimimNeighborsSTOP.AutoSize = true;
            this.label_MinimimNeighborsSTOP.Location = new System.Drawing.Point(965, 123);
            this.label_MinimimNeighborsSTOP.Name = "label_MinimimNeighborsSTOP";
            this.label_MinimimNeighborsSTOP.Size = new System.Drawing.Size(13, 13);
            this.label_MinimimNeighborsSTOP.TabIndex = 99;
            this.label_MinimimNeighborsSTOP.Text = "4";
            // 
            // tkBar_MinimumNeighborsSTOP
            // 
            this.tkBar_MinimumNeighborsSTOP.Location = new System.Drawing.Point(1021, 115);
            this.tkBar_MinimumNeighborsSTOP.Maximum = 20;
            this.tkBar_MinimumNeighborsSTOP.Minimum = 1;
            this.tkBar_MinimumNeighborsSTOP.Name = "tkBar_MinimumNeighborsSTOP";
            this.tkBar_MinimumNeighborsSTOP.Size = new System.Drawing.Size(104, 45);
            this.tkBar_MinimumNeighborsSTOP.TabIndex = 100;
            this.tkBar_MinimumNeighborsSTOP.Value = 4;
            this.tkBar_MinimumNeighborsSTOP.Scroll += new System.EventHandler(this.tkBar_MinimumNeighborsSTOP_Scroll);
            // 
            // SpeedSignRecognizer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1185, 687);
            this.Controls.Add(this.tkBar_MinimumNeighborsSTOP);
            this.Controls.Add(this.label_MinimimNeighborsSTOP);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.checkBox_RectanglesAndTriangles);
            this.Controls.Add(this.pictureBox_stop);
            this.Controls.Add(this.label_GapBetweenLines);
            this.Controls.Add(this.label_MinLineWidth);
            this.Controls.Add(this.tkBar_GapBetweenLines);
            this.Controls.Add(this.tkBar_MinLineLength);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label_ThresholdHoughLines);
            this.Controls.Add(this.tkBar_ThresholdHoughLines);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label_CannyThresholdLinkingLines);
            this.Controls.Add(this.label_CannyMinThresholdLines);
            this.Controls.Add(this.tkBar_CannyThresholdLinkingLines);
            this.Controls.Add(this.tkBar_CannyMinThresholdLines);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.checkBox_HaarCascadeForSpeedSigns);
            this.Controls.Add(this.checkBox_lines);
            this.Controls.Add(this.pictureBox_sfarsit_restrictii);
            this.Controls.Add(this.pictureBox_giveWay);
            this.Controls.Add(this.pictureBox_interzis_ambele);
            this.Controls.Add(this.pictureBox_interzis);
            this.Controls.Add(this.pictureBox_limit_100);
            this.Controls.Add(this.pictureBox_limit_90);
            this.Controls.Add(this.pictureBox_limit_70);
            this.Controls.Add(this.checkBox_HSV);
            this.Controls.Add(this.tkBar_MinDistanceBtwCenters);
            this.Controls.Add(this.label_MinDIstanceBtwCenters);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label_ReAccCenter);
            this.Controls.Add(this.tkBar_RessAccCenter);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.tkBar_CircleAccumulatorTreshold);
            this.Controls.Add(this.label_CircleAccumulatorTreshold);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label_CannyTreshold);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.tkBar_CannyTreshold);
            this.Controls.Add(this.label_MaxValue);
            this.Controls.Add(this.label_MinValue);
            this.Controls.Add(this.tkBar_MaximumValue);
            this.Controls.Add(this.tkBar_MinimumValue);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label_MaxSaturation);
            this.Controls.Add(this.label_MinSaturation);
            this.Controls.Add(this.tkBar_MaximumSaturation);
            this.Controls.Add(this.tkBar_MinimumSaturation);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label_MaxHue);
            this.Controls.Add(this.label_MinHue);
            this.Controls.Add(this.tkBar_MaximumHue);
            this.Controls.Add(this.tkBar_MinimumHue);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox_Value);
            this.Controls.Add(this.pictureBox_Saturation);
            this.Controls.Add(this.pictureBox_Hue);
            this.Controls.Add(this.pictureBox_cropped_circle);
            this.Controls.Add(this.pictureBox_limit_10);
            this.Controls.Add(this.pictureBox_ColoredCroppedSign);
            this.Controls.Add(this.pictureBox_limit_130);
            this.Controls.Add(this.pictureBox_limit_120);
            this.Controls.Add(this.pictureBox_limit_30);
            this.Controls.Add(this.pictureBox_limit_50);
            this.Controls.Add(this.tkBar_MaximumDetectionScale);
            this.Controls.Add(this.label_MaximumDetectionScale);
            this.Controls.Add(this.label_MinimumDetectionScale);
            this.Controls.Add(this.tkBar_MinumumDetectionScale);
            this.Controls.Add(this.label_MinimumNeighbors);
            this.Controls.Add(this.tkBar_MinimumNeighbors);
            this.Controls.Add(this.labelMaxDetectionScale);
            this.Controls.Add(this.comboBoxScaleIncRate);
            this.Controls.Add(this.labelMinDetectionScale);
            this.Controls.Add(this.labelMinNeighbors);
            this.Controls.Add(this.labelScaleIncreaseRate);
            this.Controls.Add(this.labelTitle);
            this.Controls.Add(this.btnBrowse);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.CamImageBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(1201, 726);
            this.MinimumSize = new System.Drawing.Size(1201, 726);
            this.Name = "SpeedSignRecognizer";
            this.Text = "Speed sign recognizer";
            this.Load += new System.EventHandler(this.RoadSignRecognizer_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tkBar_MinimumNeighbors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tkBar_MinumumDetectionScale)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tkBar_MaximumDetectionScale)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_limit_10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_ColoredCroppedSign)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_limit_130)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_limit_120)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_limit_30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_limit_50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CamImageBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_cropped_circle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Hue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Saturation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Value)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tkBar_MinimumHue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tkBar_MaximumHue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tkBar_MinimumSaturation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tkBar_MaximumSaturation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tkBar_MinimumValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tkBar_MaximumValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tkBar_CannyTreshold)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tkBar_CircleAccumulatorTreshold)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tkBar_RessAccCenter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tkBar_MinDistanceBtwCenters)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_limit_70)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_limit_90)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_limit_100)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_interzis)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_interzis_ambele)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_giveWay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_sfarsit_restrictii)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tkBar_CannyMinThresholdLines)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tkBar_CannyThresholdLinkingLines)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tkBar_ThresholdHoughLines)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tkBar_MinLineLength)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tkBar_GapBetweenLines)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_stop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tkBar_MinimumNeighborsSTOP)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Emgu.CV.UI.ImageBox CamImageBox;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.Label labelScaleIncreaseRate;
        private System.Windows.Forms.Label labelMinNeighbors;
        private System.Windows.Forms.Label labelMinDetectionScale;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.ComboBox comboBoxScaleIncRate;
        private System.Windows.Forms.Label labelMaxDetectionScale;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.TrackBar tkBar_MinimumNeighbors;
        private System.Windows.Forms.Label label_MinimumNeighbors;
        private System.Windows.Forms.TrackBar tkBar_MinumumDetectionScale;
        private System.Windows.Forms.Label label_MinimumDetectionScale;
        private System.Windows.Forms.Label label_MaximumDetectionScale;
        private System.Windows.Forms.TrackBar tkBar_MaximumDetectionScale;
        private System.Windows.Forms.PictureBox pictureBox_limit_50;
        private System.Windows.Forms.PictureBox pictureBox_limit_30;
        private System.Windows.Forms.PictureBox pictureBox_limit_120;
        private System.Windows.Forms.PictureBox pictureBox_limit_130;
        private System.Windows.Forms.PictureBox pictureBox_ColoredCroppedSign;
        private System.Windows.Forms.PictureBox pictureBox_limit_10;
        private System.Windows.Forms.PictureBox pictureBox_cropped_circle;
        private System.Windows.Forms.PictureBox pictureBox_Hue;
        private System.Windows.Forms.PictureBox pictureBox_Saturation;
        private System.Windows.Forms.PictureBox pictureBox_Value;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TrackBar tkBar_MinimumHue;
        private System.Windows.Forms.TrackBar tkBar_MaximumHue;
        private System.Windows.Forms.Label label_MinHue;
        private System.Windows.Forms.Label label_MaxHue;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TrackBar tkBar_MinimumSaturation;
        private System.Windows.Forms.TrackBar tkBar_MaximumSaturation;
        private System.Windows.Forms.Label label_MinSaturation;
        private System.Windows.Forms.Label label_MaxSaturation;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TrackBar tkBar_MinimumValue;
        private System.Windows.Forms.TrackBar tkBar_MaximumValue;
        private System.Windows.Forms.Label label_MinValue;
        private System.Windows.Forms.Label label_MaxValue;
        private System.Windows.Forms.TrackBar tkBar_CannyTreshold;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label_CannyTreshold;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label_CircleAccumulatorTreshold;
        private System.Windows.Forms.TrackBar tkBar_CircleAccumulatorTreshold;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TrackBar tkBar_RessAccCenter;
        private System.Windows.Forms.Label label_ReAccCenter;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label_MinDIstanceBtwCenters;
        private System.Windows.Forms.TrackBar tkBar_MinDistanceBtwCenters;
        private System.Windows.Forms.CheckBox checkBox_HSV;
        private System.Windows.Forms.PictureBox pictureBox_limit_70;
        private System.Windows.Forms.PictureBox pictureBox_limit_90;
        private System.Windows.Forms.PictureBox pictureBox_limit_100;
        private System.Windows.Forms.PictureBox pictureBox_interzis;
        private System.Windows.Forms.PictureBox pictureBox_interzis_ambele;
        private System.Windows.Forms.PictureBox pictureBox_giveWay;
        private System.Windows.Forms.PictureBox pictureBox_sfarsit_restrictii;
        private System.Windows.Forms.CheckBox checkBox_lines;
        private System.Windows.Forms.CheckBox checkBox_HaarCascadeForSpeedSigns;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TrackBar tkBar_CannyMinThresholdLines;
        private System.Windows.Forms.TrackBar tkBar_CannyThresholdLinkingLines;
        private System.Windows.Forms.Label label_CannyMinThresholdLines;
        private System.Windows.Forms.Label label_CannyThresholdLinkingLines;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TrackBar tkBar_ThresholdHoughLines;
        private System.Windows.Forms.Label label_ThresholdHoughLines;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TrackBar tkBar_MinLineLength;
        private System.Windows.Forms.TrackBar tkBar_GapBetweenLines;
        private System.Windows.Forms.Label label_MinLineWidth;
        private System.Windows.Forms.Label label_GapBetweenLines;
        private System.Windows.Forms.PictureBox pictureBox_stop;
        private System.Windows.Forms.CheckBox checkBox_RectanglesAndTriangles;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label_MinimimNeighborsSTOP;
        private System.Windows.Forms.TrackBar tkBar_MinimumNeighborsSTOP;
    }
}

