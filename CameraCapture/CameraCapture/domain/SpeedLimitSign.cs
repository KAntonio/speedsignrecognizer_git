﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoadSignRecognizer.domain
{
    class SpeedLimitSign : IRoadSign
    {
        public double centerX { get; set; }
        public double centerY { get; set; }
        public int speed { get; set; }

        private String[] possibleSpeeds;
        int index;

        public SpeedLimitSign()
        {
            this.speed = -1;
            index = 0;
        }

        public void AllocatePossibleSpeeds(int numberOfCircles)
        {
            possibleSpeeds = new String[numberOfCircles];
        }

        public void AddPossibleSpeed(String speed)
        {
            possibleSpeeds[index] = speed;
            index++;
        }

        public void setPossibleSpeeds(String[] s)
        {
            this.possibleSpeeds = s;
        }

        public String[] GetPossibleSpeeds()
        {
            return possibleSpeeds;
        }
    }
}
