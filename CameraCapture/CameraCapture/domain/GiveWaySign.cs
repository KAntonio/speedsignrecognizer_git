﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RoadSignRecognizer.domain;

namespace CameraCapture.domain
{
    class GiveWaySign : IRoadSign
    {
       public  double centerX { get; set; }
       public  double centerY { get; set; }

        public GiveWaySign() 
        {
            centerX = 0.0;
            centerY = 0.0;
        }

        public GiveWaySign(double x, double y)
        {
            centerX = x;
            centerY = y;
        }
    }
}
