﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using RoadSignRecognizer.domain;

namespace CameraCapture.domain
{
    class StopSign: IRoadSign
    {
        public double centerX { get; set; }
        public double centerY { get; set; }

        public StopSign()
        {
            centerX = 0.0;
            centerY = 0.0;
        }

        public StopSign(double x, double y)
        {
            centerX = x;
            centerY = y;
        }
    }
}
