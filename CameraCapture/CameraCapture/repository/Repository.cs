﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using RoadSignRecognizer.domain;
using CameraCapture.domain;


namespace CameraCapture.repository
{
    class Repository
    {
        public bool found_10 { get; set; }
        public bool found_30 { get; set; }
        public bool found_50 { get; set; }
        public bool found_70 { get; set; }
        public bool found_90 { get; set; }
        public bool found_100 { get; set; }
        public bool found_120 { get; set; }
        public bool found_130 { get; set; }
        public bool found_giveWay { get; set; }
        public bool found_noEntry { get; set; }
        public bool found_stop { get; set; }

        private List<SpeedLimitSign> _listOfSpeedLimitSigns;
        private List<GiveWaySign> _listOfGiveWaySigns;
        private List<NoEntrySign> _listOfNoEntrySigns;
        private List<StopSign> _listOfStopSigns;


        public Repository()
        {
            found_10 = false;
            found_30 = false;
            found_50 = false;
            found_70 = false;
            found_90 = false;
            found_100 = false;
            found_120 = false;
            found_130 = false;
            found_giveWay = false;
            found_noEntry = false;
            found_stop = false;

            _listOfSpeedLimitSigns = new List<SpeedLimitSign>();
            _listOfGiveWaySigns = new List<GiveWaySign>();
            _listOfNoEntrySigns = new List<NoEntrySign>();
            _listOfStopSigns = new List<StopSign>();
        }
        
        public void SetListOfStopSigns(List<StopSign> list)
        {
            this._listOfStopSigns = list;
        }
        public List<StopSign> GetListOfStopSigns()
        {
            return this._listOfStopSigns;
        }

        public void SetListOfNoEntrySigns(List<NoEntrySign> list)
        {
            this._listOfNoEntrySigns = list;
        }
        public List<NoEntrySign> GetListOfNoEntrySigns()
        {
            return this._listOfNoEntrySigns;
        }
        public void SetListOfSpeedLimitSigns(List<SpeedLimitSign> list)
        {
            this._listOfSpeedLimitSigns = list;
        }

        public List<SpeedLimitSign> GetListOfSpeedLimitSigns()
        {
            return this._listOfSpeedLimitSigns;
        }

        public void SetListOfGiveWaySigns(List<GiveWaySign> list)
        {
            this._listOfGiveWaySigns = list;
        }
        public List<GiveWaySign> GetListOfGiveWaySigns()
        {
            return this._listOfGiveWaySigns;
        }


        public void AddStopSign(StopSign stop)
        {
            this._listOfStopSigns.Add(stop);
        }
        public void AddNoEntrySign(NoEntrySign noEntrySign)
        {
            this._listOfNoEntrySigns.Add(noEntrySign);
        }
        public void AddGiveWaySign(GiveWaySign giveWaySign)
        {
            this._listOfGiveWaySigns.Add(giveWaySign);
        }

        public void AddSpeedSign(SpeedLimitSign speedSign)
        {
            this._listOfSpeedLimitSigns.Add(speedSign);
        }

        public void ResetRepository()
        {
            this._listOfSpeedLimitSigns.Clear();  // clearing the list for the next frame
            this._listOfGiveWaySigns.Clear();
            this._listOfNoEntrySigns.Clear();
            this._listOfStopSigns.Clear();
            found_10 = false;
            found_30 = false;
            found_50 = false;
            found_70 = false;
            found_90 = false;
            found_100 = false;
            found_120 = false;
            found_130 = false;
            found_giveWay = false;
            found_noEntry = false;
            found_stop = false;
        }

        public int GetLengthOfStopSignsList()
        {
            return this._listOfStopSigns.Count;
        }

        public int GetLengthOfSpeedSignsList()
        {
            return this._listOfSpeedLimitSigns.Count;
        }

        public int GetLengthOfGetWaySignsList()
        {
            return this._listOfGiveWaySigns.Count;
        }
        public int GetLengthOfNoEntrySignsList()
        {
            return this._listOfNoEntrySigns.Count;
        }

        public void setBoolFoundStopSign()
        {
            if(GetLengthOfStopSignsList() > 0)
            {
                this.found_stop = true;
            }
        }
        public void SetBoolFoundGiveWaySign()
        {
            if(GetLengthOfGetWaySignsList() > 0)
            {
                this.found_giveWay = true;
            }
        }

        public void SetBoolFoundNoEntrySign()
        {
            if(GetLengthOfNoEntrySignsList() > 0)
            {
                this.found_noEntry = true;
            }
        }


        public void SetBoolsFoundSpeedSigns()
        {

            int speed = -1;

            foreach (SpeedLimitSign sign in _listOfSpeedLimitSigns)  // parcurg semnele
            {
                String[] arrayOfString = sign.GetPossibleSpeeds();
                for (int i = 0; i < arrayOfString.Length; i++)
                {
                    try
                    {
                        speed = Int32.Parse(arrayOfString[i]);
                        if(speed == 10) found_10 = true  ;
                        else if(speed == 30) found_30 = true;
                        else if (speed == 50) found_50 = true;
                        else if (speed == 70) found_70 = true;
                        else if (speed == 90) found_90 = true;
                        else if (speed == 100) found_100 = true;
                        else if (speed == 120) found_120 = true;
                        else if (speed == 130) found_130 = true;

                    }
                    catch(Exception e) { }
                }
            }
        }


      



    }


}
